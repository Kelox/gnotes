#include "gn_drawable_line.h"
#include "common_funcs.h"
#include <math.h>


// STRUCTS //
typedef struct
{
    float x;
    float y;
} LinePoint;


// PROTOTYPES //
static void
gn_drawable_line_dispose(GObject* self_object);

static void
gn_drawable_line_finalize(GObject* self_object);

static void
on_draw(GnDrawable* self_drawable);

static void
on_snapshot(    GnDrawable*     self_drawable,
                GtkSnapshot*    snap);

static void
finish_construction_phase(GnDrawableLine* self);


// OBJECT CONSTRUCTION //
struct _GnDrawableLine
{
    GnDrawable parent;
    
    GSList* constr_point_list;
    
    float* list;
    int list_size;
    
    GdkRGBA line_color;
    float line_width;
};
G_DEFINE_TYPE(GnDrawableLine, gn_drawable_line, GN_TYPE_DRAWABLE)

static void
gn_drawable_line_class_init(GnDrawableLineClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &gn_drawable_line_dispose;
    G_OBJECT_CLASS(klass)->finalize = &gn_drawable_line_finalize;
    
    GN_DRAWABLE_CLASS(klass)->on_draw = &on_draw;
    GN_DRAWABLE_CLASS(klass)->on_snapshot = &on_snapshot;
}

static void
gn_drawable_line_init(GnDrawableLine* self)
{
    self->constr_point_list = NULL;
    
    self->list = NULL;
    self->list_size = 0;
    
    self->line_color = (GdkRGBA){ 0.3, 0.3, 0.3, 1.0 };
    self->line_width = 4.0f;
    
    gn_drawable_set_use_render_node(GN_DRAWABLE(self), FALSE);
    gn_drawable_set_always_draw(GN_DRAWABLE(self), TRUE);
}


// OBJECT DECONSTRUCTION //
static void
gn_drawable_line_dispose(GObject* self_object)
{
    GnDrawableLine* self = GN_DRAWABLE_LINE(self_object);
    
    if (self->list != NULL)
    {
        free(self->list);
        self->list = NULL;
    }
    
    G_OBJECT_CLASS(gn_drawable_line_parent_class)->dispose(self_object);
}

static void
gn_drawable_line_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_drawable_line_parent_class)->finalize(self_object);
}


// METHODS //
GnDrawableLine*
gn_drawable_line_new()
{
    GnDrawableLine* self = GN_DRAWABLE_LINE(g_object_new(GN_TYPE_DRAWABLE_LINE, NULL));
    
    return self;
}


void
gn_drawable_line_append_point(  GnDrawableLine* self,
                                float           x,
                                float           y)
{
    LinePoint* p = (LinePoint*)malloc(sizeof(LinePoint));
    if (!p)
        return;
    
    p->x = x;
    p->y = y;
    
    self->constr_point_list = g_slist_append(self->constr_point_list, p);
}


void
gn_drawable_line_set_point_list(    GnDrawableLine* self,
                                    GSList*         list)
{
    // TODO:
}


void
gn_drawable_line_set_point_list_packed( GnDrawableLine* self,
                                        float*          list,
                                        int             list_size)
{
    // TODO: 
}


void
gn_drawable_line_end(GnDrawableLine* self)
{
    // Malloc
    int len = g_slist_length(self->constr_point_list) * 2;
    float* packed_list = (float*)malloc(sizeof(float) * len);
    if (!packed_list)
    {
        g_error("Failed to allocate packed list");
        return;
    }
    
    // Fill list
    for (int i = 0; i < g_slist_length(self->constr_point_list); i++)
    {
        LinePoint* p = g_slist_nth_data(self->constr_point_list, i);
        packed_list[i * 2] = p->x;
        packed_list[i * 2 + 1] = p->y;
    }
    
    self->list = packed_list;
    self->list_size = len;
    
    // Finish
    finish_construction_phase(self);
}

bool
gn_drawable_line_contains_circle(   GnDrawableLine* self,
                                    float           x,
                                    float           y,
                                    float           radius)
{
    GnDrawable* self_drawable = GN_DRAWABLE(self);
    
    float pos_x = gn_drawable_get_x(self_drawable);
    float pos_y = gn_drawable_get_y(self_drawable);
    float width = gn_drawable_get_width(self_drawable);
    float height = gn_drawable_get_height(self_drawable);
    
    float dst = radius + self->line_width / 2;
    
    if ((x + dst) >= pos_x && (x - dst) <= (pos_x + width) && (y + dst) >= pos_y && (y - dst) <= (pos_y + height))
    {
        for (int i = 0; i < self->list_size; i += 2)
        {
            int next_i = i + 2;
            if (next_i >= self->list_size)
                next_i = i;
            
            if (common_dist_to_line(x, y, self->list[i] + pos_x, self->list[i + 1] + pos_y, self->list[next_i] + pos_x, self->list[next_i + 1] + pos_y) <= dst)
                return TRUE;
        }
    }
    
    return FALSE;
}

void
gn_drawable_line_set_color( GnDrawableLine* self,
                            float           red,
                            float           green,
                            float           blue,
                            float           alpha)
{
    self->line_color.red = red;
    self->line_color.green = green;
    self->line_color.blue = blue;
    self->line_color.alpha = alpha;
}

GdkRGBA*
gn_drawable_line_get_color(GnDrawableLine* self)
{
    return &self->line_color;
}



// VIRTUAL METHODS //

static void
on_draw(GnDrawable* self_drawable)
{
    GnDrawableLine* self = GN_DRAWABLE_LINE(self_drawable);
    
    
    // Free old render node
    GskRenderNode* render_node = gn_drawable_get_render_node(self_drawable);
    if (render_node)
    {
        g_object_unref(render_node);
        render_node = NULL;
    }
    
    // Create new render node
    render_node = gsk_cairo_node_new(&GRAPHENE_RECT_INIT(0, 0, gn_drawable_get_width(self_drawable), gn_drawable_get_height(self_drawable)));
    
    
    // Draw
    if (self->list_size > 0)
    {
        cairo_t* cairo = gsk_cairo_node_get_draw_context(render_node);
        
        //cairo_translate(cairo, -gn_drawable_get_x(self_drawable), -gn_drawable_get_y(self_drawable));
        cairo_set_source_rgba(cairo, self->line_color.red, self->line_color.green, self->line_color.blue, self->line_color.alpha);
        cairo_set_line_width(cairo, self->line_width);
        cairo_set_line_cap(cairo, CAIRO_LINE_CAP_ROUND);
        
        cairo_move_to(cairo, self->list[0], self->list[1]);
        for (int i = 0; i < self->list_size; i += 2)
            cairo_line_to(cairo, self->list[i], self->list[i + 1]);
        
        cairo_stroke(cairo);
        
        cairo_destroy(cairo);
    }
    
    
    // Set new render node
    gn_drawable_set_render_node(self_drawable, render_node);
}


static void
on_snapshot(    GnDrawable*     self_drawable,
                GtkSnapshot*    snap)
{
    GnDrawableLine* self = GN_DRAWABLE_LINE(self_drawable);
    GnCanvas* canvas = gn_drawable_get_canvas(self_drawable);
    
    if (g_slist_length(self->constr_point_list) == 0)
        return;
    
    // Translate & scale
    //gtk_snapshot_translate(snap, &GRAPHENE_POINT_INIT(-gn_canvas_get_translation_x(canvas), -gn_canvas_get_translation_y(canvas)));
    gtk_snapshot_scale(snap, gn_canvas_get_scale(canvas), gn_canvas_get_scale(canvas));
    
    // Add cairo
    float trans_x = gn_canvas_get_translation_x(canvas);
    float trans_y = gn_canvas_get_translation_y(canvas);
    float width = gtk_widget_get_allocated_width(GTK_WIDGET(canvas)) / gn_canvas_get_scale(canvas);
    float height = gtk_widget_get_allocated_height(GTK_WIDGET(canvas)) / gn_canvas_get_scale(canvas);
    cairo_t* cairo = gtk_snapshot_append_cairo(snap, &GRAPHENE_RECT_INIT(0, 0, width, height));
    
    // Config cairo
    cairo_translate(cairo, -trans_x, -trans_y);
    
    cairo_set_source_rgba(cairo, self->line_color.red, self->line_color.green, self->line_color.blue, self->line_color.alpha);
    cairo_set_line_width(cairo, self->line_width);
    cairo_set_line_cap(cairo, CAIRO_LINE_CAP_ROUND);
    
    // Draw line
    GSList* list = self->constr_point_list;
    LinePoint* prev = (LinePoint*)list->data;
    
    cairo_move_to(cairo, prev->x, prev->y);
    
    while (list != NULL)
    {
        LinePoint* point = (LinePoint*)list->data;
        cairo_line_to(cairo, point->x, point->y);
        
        // Next
        prev = point;
        list = list->next;
    }
    
    // Stroke
    cairo_stroke(cairo);
    
    // Done
    cairo_destroy(cairo);
}



// PRIVATE FUNCTIONS //
static void
finish_construction_phase(GnDrawableLine* self)
{
    // Clear list
    for (int i = 0; i < g_slist_length(self->constr_point_list); i++)
        free(g_slist_nth_data(self->constr_point_list, i));
    
    g_slist_free(self->constr_point_list);
    self->constr_point_list = NULL;
    
    // Calculate bounds
    float min_x = 9999999, max_x = -9999999, min_y = 9999999, max_y = -9999999;
    for (int i = 0; i < self->list_size; i += 2)
    {
        min_x = fmin(min_x, self->list[i]);
        max_x = fmax(max_x, self->list[i]);
        min_y = fmin(min_y, self->list[i+1]);
        max_y = fmax(max_y, self->list[i+1]);
    }
    
    min_x -= self->line_width;
    min_y -= self->line_width;
    max_x += self->line_width;
    max_y += self->line_width;
    
    
    // Update properties
    gn_drawable_set_use_render_node(GN_DRAWABLE(self), TRUE);
    gn_drawable_set_always_draw(GN_DRAWABLE(self), FALSE);
    
    gn_drawable_set_x(GN_DRAWABLE(self), min_x);
    gn_drawable_set_y(GN_DRAWABLE(self), min_y);
    gn_drawable_set_width(GN_DRAWABLE(self), max_x - min_x);
    gn_drawable_set_height(GN_DRAWABLE(self), max_y - min_y);
    
    
    // Make packet-list positions relative
    for (int i = 0; i < self->list_size; i += 2)
    {
        self->list[i] -= gn_drawable_get_x(GN_DRAWABLE(self));
        self->list[i + 1] -= gn_drawable_get_y(GN_DRAWABLE(self));
    }
}


// GET/SET //

PUBLIC_GET_SET(GnDrawableLine, gn_drawable_line, line_width, float)