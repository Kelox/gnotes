#include "gn_drawable_selection.h"


// PROTOTYPES //
static void
gn_drawable_selection_dispose(GObject* self_object);

static void
gn_drawable_selection_finalize(GObject* self_object);

static void
on_frame_move(  GnDrawable* self_drawable,
                float       old_x,
                float       old_y,
                float       new_x,
                float       new_y);



// OBJECT CONSTRUCTION //
struct _GnDrawableSelection
{
    GnDrawable parent;
    
    GSList* child_list;
};
G_DEFINE_TYPE(GnDrawableSelection, gn_drawable_selection, GN_TYPE_DRAWABLE)

static void
gn_drawable_selection_class_init(GnDrawableSelectionClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &gn_drawable_selection_dispose;
    G_OBJECT_CLASS(klass)->finalize = &gn_drawable_selection_finalize;
    
    GN_DRAWABLE_CLASS(klass)->on_frame_move = &on_frame_move;
}

static void
gn_drawable_selection_init(GnDrawableSelection* self)
{
    self->child_list = NULL;
    
    gn_drawable_set_has_frame(GN_DRAWABLE(self), TRUE);
    gn_drawable_set_is_focusable(GN_DRAWABLE(self), TRUE);
    gn_drawable_set_use_render_node(GN_DRAWABLE(self), FALSE);
    
    gn_drawable_set_x(GN_DRAWABLE(self), 0);
    gn_drawable_set_y(GN_DRAWABLE(self), 0);
    gn_drawable_set_width(GN_DRAWABLE(self), 0);
    gn_drawable_set_height(GN_DRAWABLE(self), 0);
}


// OBJECT DECONSTRUCTION //
static void
gn_drawable_selection_dispose(GObject* self_object)
{
    GnDrawableSelection* self = GN_DRAWABLE_SELECTION(self_object);
    
    if (self->child_list != NULL)
    {
        g_slist_free(self->child_list);
        self->child_list = NULL;
    }
    
    G_OBJECT_CLASS(gn_drawable_selection_parent_class)->dispose(self_object);
}

static void
gn_drawable_selection_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_drawable_selection_parent_class)->finalize(self_object);
}


// METHODS //
GnDrawableSelection*
gn_drawable_selection_new()
{
    GnDrawableSelection* self = GN_DRAWABLE_SELECTION(g_object_new(GN_TYPE_DRAWABLE_SELECTION, NULL));
    
    
    return self;
}


void
gn_drawable_selection_add_drawable( GnDrawableSelection*    self,
                                    GnDrawable*             drawable)
{
    // Get new min/max values
    float self_xmin = gn_drawable_get_x(GN_DRAWABLE(self)), self_xmax = gn_drawable_get_x(GN_DRAWABLE(self)) + gn_drawable_get_width(GN_DRAWABLE(self));
    float self_ymin = gn_drawable_get_y(GN_DRAWABLE(self)), self_ymax = gn_drawable_get_y(GN_DRAWABLE(self)) + gn_drawable_get_height(GN_DRAWABLE(self));
    
    float drw_xmin = gn_drawable_get_x(drawable), drw_xmax = gn_drawable_get_x(drawable) + gn_drawable_get_width(drawable);
    float drw_ymin = gn_drawable_get_y(drawable), drw_ymax = gn_drawable_get_y(drawable) + gn_drawable_get_height(drawable);
    
    if (g_slist_length(self->child_list) == 0)
    {
        self_xmin = drw_xmin;
        self_xmax = drw_xmax;
        self_ymin = drw_ymin;
        self_ymax = drw_ymax;
    }
    
    self_xmin = fminf(self_xmin, drw_xmin);
    self_xmax = fmaxf(self_xmax, drw_xmax);
    self_ymin = fminf(self_ymin, drw_ymin);
    self_ymax = fmaxf(self_ymax, drw_ymax);
    
    // Update new values
    gn_drawable_set_x(GN_DRAWABLE(self), self_xmin);
    gn_drawable_set_y(GN_DRAWABLE(self), self_ymin);
    gn_drawable_set_width(GN_DRAWABLE(self), self_xmax - self_xmin);
    gn_drawable_set_height(GN_DRAWABLE(self), self_ymax - self_ymin);
    
    // Add to list
    self->child_list = g_slist_append(self->child_list, drawable);
}


// PRIVATE FUNCTIONS //
static void
on_frame_move(  GnDrawable* self_drawable,
                float       old_x,
                float       old_y,
                float       new_x,
                float       new_y)
{
    GnDrawableSelection* self = GN_DRAWABLE_SELECTION(self_drawable);
    
    float dx = new_x - old_x;
    float dy = new_y - old_y;
    
    for (int i = 0; i < g_slist_length(self->child_list); i++)
    {
        GnDrawable* drawable = (GnDrawable*)g_slist_nth_data(self->child_list, i);
        
        gn_drawable_set_x(drawable, gn_drawable_get_x(drawable) + dx);
        gn_drawable_set_y(drawable, gn_drawable_get_y(drawable) + dy);
    }
}