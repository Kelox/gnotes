#include "gn_canvas.h"
#include "gn_drawable_text.h"
#include "gn_drawable_line.h"
#include "gn_drawable_selection.h"
#include "common_funcs.h"
#include <gdk/gdk.h>


// MACROS //
#define MAX_ZOOM 10.0f
#define MIN_ZOOM 0.1f
#define GRID_SIZE 50


// TASKS //
enum GnCanvasTask
{
    GN_TASK_NONE = 0,
    GN_TASK_MOVE_VIEW,
    GN_TASK_DRAWABLE_DRAG,
    GN_TASK_DRAWABLE_DRAG_ROUNDED,
    GN_TASK_DRAWABLE_RESIZE,
    GN_TASK_DRAWABLE_RESIZE_ROUNDED,
    GN_TASK_DRAW,
    GN_TASK_DRAW_ROUNDED,
    GN_TASK_DRAW_ERASER,
    GN_TASK_DRAW_SELECTION
};

#define HAS_TASK(canvas)            (canvas->task != GN_TASK_NONE)
#define IS_TASK(canvas, tsk)        (canvas->task == tsk)
#define SET_TASK(canvas, tsk)       canvas->task = tsk;
#define END_TASK(canvas)            canvas->task = GN_TASK_NONE;
#define GET_TASK(canvas)            canvas->task
#define TRY_SET_TASK(canvas, tsk)   { if (!HAS_TASK(canvas)) SET_TASK(canvas, tsk) }


// TASK STRUCTS //
typedef struct
{
    bool drag_started;
    float drag_start_x;
    float drag_start_y;
    float frame_delta_x;
    float frame_delta_y;
} GnTaskDataDrag;

typedef struct
{
    bool resize_started;
    float drag_start_x;
    float right_delta_x; // distance from local x-pos to right edge
} GnTaskDataResize;

typedef struct
{
    float trans_start_x;
    float trans_start_y;
    float pixel_start_x;
    float pixel_start_y;
} GnTaskDataMoveView;

typedef struct
{
    GnDrawableLine* line;
} GnTaskDataDraw;

typedef struct
{
    // ..used for the eraser to not accidentally skip over lines
    float prev_mouse_x;
    float prev_mouse_y;
} GnTaskDataDrawMotion;

typedef struct
{
    float from_x;
    float from_y;
    float to_x;
    float to_y;
} GnTaskDataDrawSelection;

typedef union
{
    GnTaskDataDrag drag;
    GnTaskDataResize resize;
    GnTaskDataMoveView move_view;
    GnTaskDataDraw draw;
    GnTaskDataDrawMotion draw_motion;
    GnTaskDataDrawSelection draw_selection;
} GnTaskData;


// PROTOTYPES //

// ..GObject
static void
gn_canvas_dispose(GObject* self_object);

static void
gn_canvas_finalize(GObject* self_object);


// ..GtkWidget
static GtkSizeRequestMode
get_request_mode(GtkWidget* widget);

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline);

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap);


// ..Event handlers
static void
ctrl_on_motion( GtkEventControllerMotion*   motion_controller, // ..this func also handles on_enter
                gdouble                     x,
                gdouble                     y,
                gpointer                    user_data);

static void
ctrl_on_motion_leave(   GtkEventControllerMotion*   motion_controller,
                        gpointer                    user_data);

static void
ctrl_on_motion_tab_start(   GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y);

static void
ctrl_on_motion_tab_insert(  GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y);

static void
ctrl_on_motion_tab_draw(    GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y);

static void
ctrl_on_motion_tab_view(    GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y);


static gboolean
ctrl_on_legacy( GtkEventControllerLegacy*   legacy_controller,
                GdkEvent*                   event,
                gpointer                    user_data);

static void
ctrl_on_legacy_tab_start(   GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y);

static void
ctrl_on_legacy_tab_insert(  GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y);

static void
ctrl_on_legacy_tab_draw(    GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y);

static void
ctrl_on_legacy_tab_view(    GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y);


static void
gui_on_tab_changed( GnGuiController*    gui_controller,
                    enum GnGuiTab       prev_tab_id,
                    enum GnGuiTab       tab_id,
                    gpointer            user_data);

static void
gui_on_text_bold_toggled(   GnGuiController*    gui_controller,
                            bool                state,
                            gpointer            user_data);

static void
gui_on_text_italics_toggled(    GnGuiController*    gui_controller,
                                bool                state,
                                gpointer            user_data);

static void
gui_on_text_underlined_toggled( GnGuiController*    gui_controller,
                                bool                state,
                                gpointer            user_data);


// ..GnCanvas private funcs
static void
update_hovering_drawable(   GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y);

static void
drawable_take_focus(    GnCanvas*   self,
                        GnDrawable* drawable,
                        float       mouse_x,
                        float       mouse_y);



// OBJECT CONSTRUCTION //
struct _GnCanvas
{
    GtkWidget parent;
    
    // Data
    GnGuiController* gui_controller;
    GList* drawable_list; // first element lays on top and (if its below the cursor) receives all events first (-> should be drawn last)
    
    // View
    float translation_x; // position of the upper-left corner; these values are NOT scaled
    float translation_y;
    float scale; // scale is only applied AFTER translation!
    
    // State
    GnDrawable* hovering_drawable;
    GnDrawable* focused_drawable;
    enum GnCanvasTask task;
    GnTaskData task_data;
    
    float mouse_pixel_x; // always store the last known mouse pixel position
    float mouse_pixel_y;
    
    // Event Controllers
    GtkEventController* motion_controller;
    GtkEventController* legacy_controller;
};
G_DEFINE_TYPE(GnCanvas, gn_canvas, GTK_TYPE_WIDGET)

static void
gn_canvas_class_init(GnCanvasClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = gn_canvas_dispose;
    G_OBJECT_CLASS(klass)->finalize = gn_canvas_finalize;
    
    GTK_WIDGET_CLASS(klass)->get_request_mode = get_request_mode;
    GTK_WIDGET_CLASS(klass)->measure = measure;
    GTK_WIDGET_CLASS(klass)->snapshot = snapshot;
}

static void
gn_canvas_init(GnCanvas* self)
{
    gtk_widget_set_hexpand(GTK_WIDGET(self), TRUE);
    gtk_widget_set_vexpand(GTK_WIDGET(self), TRUE);
    gtk_widget_set_overflow(GTK_WIDGET(self), GTK_OVERFLOW_HIDDEN);
    
    self->drawable_list = NULL;
    
    self->translation_x = 0;
    self->translation_y = 0;
    self->scale = 1.0f;
    
    self->hovering_drawable = NULL;
    self->focused_drawable = NULL;
    self->task = GN_TASK_NONE;
    
    self->mouse_pixel_x = 0;
    self->mouse_pixel_y = 0;
    
    
    // Create event controllers
    self->motion_controller = gtk_event_controller_motion_new();
    gtk_event_controller_set_propagation_limit(self->motion_controller, GTK_LIMIT_NONE);
    gtk_event_controller_set_propagation_phase(self->motion_controller, GTK_PHASE_CAPTURE);
    
    self->legacy_controller = gtk_event_controller_legacy_new();
    gtk_event_controller_set_propagation_limit(self->legacy_controller, GTK_LIMIT_NONE);
    gtk_event_controller_set_propagation_phase(self->legacy_controller, GTK_PHASE_CAPTURE);
    
    // Connect controller signals
    g_signal_connect(self->motion_controller, "enter", G_CALLBACK(ctrl_on_motion), self);
    g_signal_connect(self->motion_controller, "motion", G_CALLBACK(ctrl_on_motion), self);
    g_signal_connect(self->motion_controller, "leave", G_CALLBACK(ctrl_on_motion_leave), self);
    g_signal_connect(self->legacy_controller, "event", G_CALLBACK(ctrl_on_legacy), self);
    
    // Add event controllers
    gtk_widget_add_controller(GTK_WIDGET(self), self->legacy_controller); // ..last added controllers trigger first
    gtk_widget_add_controller(GTK_WIDGET(self), self->motion_controller);
}



// OBJECT DECONSTRUCTION //
static void
gn_canvas_dispose(GObject* self_object)
{
    GnCanvas* self = GN_CANVAS(self_object);
    
    // Remove all drawables
    while (g_list_length(self->drawable_list) > 0)
    {
        GnDrawable* d = (GnDrawable*)g_list_nth_data(self->drawable_list, 0);
        gn_canvas_remove_drawable(self, d);
        g_object_unref(d);
    }
    
    
    G_OBJECT_CLASS(gn_canvas_parent_class)->dispose(self_object);
}

static void
gn_canvas_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_canvas_parent_class)->finalize(self_object);
}



// METHODS //
GnCanvas*
gn_canvas_new(GnGuiController* gui_controller)
{
    GnCanvas* self = GN_CANVAS(g_object_new(GN_TYPE_CANVAS, NULL));
    
    self->gui_controller = gui_controller;
    
    g_signal_connect(   self->gui_controller,
                        "tab-changed",
                        G_CALLBACK(gui_on_tab_changed),
                        self);
    
    g_signal_connect(self->gui_controller, "toggle-text-bold", G_CALLBACK(gui_on_text_bold_toggled), self);
    g_signal_connect(self->gui_controller, "toggle-text-italic", G_CALLBACK(gui_on_text_italics_toggled), self);
    g_signal_connect(self->gui_controller, "toggle-text-underline", G_CALLBACK(gui_on_text_underlined_toggled), self);
    
    return self;
}

void
gn_canvas_append_drawable(  GnCanvas*   self,
                            GnDrawable* drawable)
{
    self->drawable_list = g_list_prepend(self->drawable_list, drawable);
    gn_drawable_set_canvas(drawable, self);
    
    GN_DRAWABLE_GET_CLASS(drawable)->on_canvas_append(drawable);
}

void
gn_canvas_remove_drawable(  GnCanvas*   self,
                            GnDrawable* drawable)
{
    gn_drawable_set_canvas(drawable, NULL);
    self->drawable_list = g_list_remove(self->drawable_list, drawable);
    
    GN_DRAWABLE_GET_CLASS(drawable)->on_canvas_remove(drawable);
}

GList*
gn_canvas_get_drawable_list(GnCanvas* self)
{
    return self->drawable_list;
}

void
gn_canvas_pixel_to_unit(    GnCanvas*   self,
                            float*      x,
                            float*      y)
{
    *x = *x / self->scale + self->translation_x;
    *y = *y / self->scale + self->translation_y;
}

void
gn_canvas_unit_to_pixel(    GnCanvas*   self,
                            float*      x,
                            float*      y)
{
    *x = (*x - self->translation_x) * self->scale;
    *y = (*y - self->translation_y) * self->scale;
}

GnGuiController*
gn_canvas_get_gui_controller(GnCanvas* self)
{
    return self->gui_controller;
}



// PRIVATE FUNCTIONS //

// Widget functions
static GtkSizeRequestMode
get_request_mode(GtkWidget* widget)
{
    return GTK_SIZE_REQUEST_CONSTANT_SIZE;
}

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline)
{
    if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
        *minimum = 400;
        *natural = 600;
    }
    else
    {
        *minimum = 200;
        *natural = 400;
    }
}

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap)
{
    // Get self
    GnCanvas* self = GN_CANVAS(self_widget);
    
    
    // Get allocation
    GtkAllocation alloc;
    gtk_widget_get_allocation(self_widget, &alloc);
    
    
    // Draw grid
    {
        cairo_t* cairo = gtk_snapshot_append_cairo(snap, &GRAPHENE_RECT_INIT(0, 0, alloc.width, alloc.height));
        
        float zoom_mult = 1.0f;
        if (self->scale < 1.0f)
            zoom_mult = ((self->scale - MIN_ZOOM) / (1.0f - MIN_ZOOM)) * 0.9f + 0.1f;
        
        for (float x = floor(self->translation_x / GRID_SIZE) * GRID_SIZE; x < (self->translation_x + alloc.width / self->scale); x += GRID_SIZE)
        {
            cairo_move_to(cairo, (x - self->translation_x) * self->scale, 0);
            cairo_line_to(cairo, (x - self->translation_x) * self->scale, alloc.height);
            
            cairo_set_source_rgba(cairo, 0.5 * (x == 0 ? 1 : 0), 0, 0, ((x == 0 || ((int)x % (GRID_SIZE * 10)) == 0) ? 1.0 : zoom_mult));
            cairo_set_line_width(cairo, (x == 0 ? 3.0 : 1.0));
            cairo_stroke(cairo);
        }
        
        for (float y = floor(self->translation_y / GRID_SIZE) * GRID_SIZE; y < (self->translation_y + alloc.height / self->scale); y += GRID_SIZE)
        {
            cairo_move_to(cairo, 0, (y - self->translation_y) * self->scale);
            cairo_line_to(cairo, alloc.width, (y - self->translation_y) * self->scale);
            
            cairo_set_source_rgba(cairo, 0.5 * (y == 0 ? 1 : 0), 0, 0, ((y == 0 || ((int)y % (GRID_SIZE * 10)) == 0) ? 1.0 : zoom_mult));
            cairo_set_line_width(cairo, (y == 0 ? 3.0 : 1.0));
            cairo_stroke(cairo);
        }
        
        cairo_destroy(cairo);
    }
    
    
    // Draw render nodes
    for (int i = (g_list_length(self->drawable_list) - 1); i >= 0; i--)
    {
        // Get drawable
        GnDrawable* drawable = (GnDrawable*)g_list_nth_data(self->drawable_list, i);
        
        // Skip if not visible anyways
        if (!gn_drawable_get_always_draw(drawable) && !gn_drawable_contains_rect(drawable, self->translation_x, self->translation_y, alloc.width / self->scale, alloc.height / self->scale))
            continue;
        
        // Get render node
        GskRenderNode* render_node = NULL;
        
        if (gn_drawable_get_use_render_node(drawable))
        {
            // ..get render node
            render_node = gn_drawable_get_render_node(drawable);
            
            // ..draw if there is no render node
            if (!render_node)
            {
                GN_DRAWABLE_GET_CLASS(drawable)->on_draw(drawable);
                render_node = gn_drawable_get_render_node(drawable);
            }
            
            // ..skip if there STILL isn't a render node
            if (!render_node)
            {
                g_warning("Render-node still missing after draw (%p)", drawable);
                continue;
            }
        }
        
        
        // Transform to render node
        gtk_snapshot_save(snap);
        
        if (gn_drawable_get_use_render_node(drawable))
        {
            gtk_snapshot_scale(snap, self->scale, self->scale);
            gtk_snapshot_translate(snap, &GRAPHENE_POINT_INIT(-self->translation_x, -self->translation_y));
            gtk_snapshot_translate(snap, &GRAPHENE_POINT_INIT(gn_drawable_get_x(drawable), gn_drawable_get_y(drawable)));
            
            gtk_snapshot_append_node(snap, render_node);
        }
        else
        {
            GN_DRAWABLE_GET_CLASS(drawable)->on_snapshot(drawable, snap);
        }
        
        
        // Restore transformation
        gtk_snapshot_restore(snap);
        
        
        // Draw frame if hovered or focused
        if (self->hovering_drawable == drawable || self->focused_drawable == drawable)
        {
            gn_drawable_draw_frame( drawable,
                                    snap,
                                    self->translation_x,
                                    self->translation_y,
                                    self->scale,
                                    (self->focused_drawable == drawable ? 1.0f : 0.6f));
        }
    }
    
    
    // Draw current drawing tool
    if (gn_gui_controller_get_tab(self->gui_controller) == GN_GUI_TAB_DRAW && gn_gui_controller_get_draw_tool(self->gui_controller) != GN_DRAW_TOOL_SELECTION)
    {
        // Initial values
        float radius = gn_gui_controller_get_draw_width(self->gui_controller) / 2;
        
        float mx = self->mouse_pixel_x, my = self->mouse_pixel_y;
        gn_canvas_pixel_to_unit(self, &mx, &my);
        
        
        // Get/create render node
        static GskRenderNode* circle_render_node = NULL;
        static float circle_render_node_size = 0;
        if (circle_render_node && circle_render_node_size != (self->scale * radius * 2))
        {
            gsk_render_node_unref(circle_render_node);
            circle_render_node = NULL;
        }
        
        if (!circle_render_node)
        {
            // Create render node
            circle_render_node_size = (self->scale * radius * 2);
            circle_render_node = gsk_cairo_node_new(&GRAPHENE_RECT_INIT(0, 0, circle_render_node_size, circle_render_node_size));
            
            
            // Draw
            cairo_t* cairo = gsk_cairo_node_get_draw_context(circle_render_node);
            
            cairo_set_source_rgba(cairo, 0.3, 0.3, 0.3, 0.7);
            cairo_set_line_width(cairo, 1);
            cairo_arc(cairo, radius, radius, radius - 0.5f, 0, M_PI * 2);
            
            cairo_stroke(cairo);
            
            cairo_destroy(cairo);
        }
        
        
        // Append to snapshot
        gtk_snapshot_save(snap);
        
        gtk_snapshot_scale(snap, self->scale, self->scale);
        gtk_snapshot_translate(snap, &GRAPHENE_POINT_INIT(-self->translation_x, -self->translation_y));
        gtk_snapshot_translate(snap, &GRAPHENE_POINT_INIT(mx - radius, my - radius));
        
        gtk_snapshot_append_node(snap, circle_render_node);
        
        
        // Done
        gtk_snapshot_restore(snap);
    }
    
    
    // Draw selection task
    if (IS_TASK(self, GN_TASK_DRAW_SELECTION))
    {
        // Create cairo
        cairo_t* cairo = gtk_snapshot_append_cairo(snap, &GRAPHENE_RECT_INIT(0, 0, alloc.width, alloc.height));
        
        // Get from & to
        float from_x = self->task_data.draw_selection.from_x, from_y = self->task_data.draw_selection.from_y;
        float to_x = self->task_data.draw_selection.to_x, to_y = self->task_data.draw_selection.to_y;
        gn_canvas_unit_to_pixel(self, &from_x, &from_y);
        gn_canvas_unit_to_pixel(self, &to_x, &to_y);
        
        // ..change values to avoid negative size
        if (from_x > to_x)
        {
            float ph = from_x;
            from_x = to_x;
            to_x = ph;
        }
        if (from_y > to_y)
        {
            float ph = from_y;
            from_y = to_y;
            to_y = ph;
        }
        
        
        // Draw rectangle
        for (int i = 0; i < 2; i++)
        {
            cairo_rectangle(cairo, from_x, from_y, to_x - from_x, to_y - from_y);
            
            if (i == 0)
            {
                cairo_set_source_rgba(cairo, 0.2, 0.4, 1.0, 0.5);
                cairo_fill(cairo);
            }
            else
            {
                cairo_set_line_width(cairo, 2.0f);
                cairo_set_source_rgb(cairo, 0.2, 0.4, 1.0);
                cairo_stroke(cairo);
            }
        }
        
        // Done
        cairo_destroy(cairo);
    }
}


// Event controller functions
static void
ctrl_on_motion( GtkEventControllerMotion*   motion_controller, // ..this func also handles on_enter
                gdouble                     x,
                gdouble                     y,
                gpointer                    user_data)
{
    GnCanvas* self = (GnCanvas*)user_data;
    
    float mouse_x = x, mouse_y = y;
    gn_canvas_pixel_to_unit(self, &mouse_x, &mouse_y);
    
    
    // Update currently hovering drawable
    if (gn_gui_controller_is_tab(self->gui_controller, GN_GUI_TAB_START))
        update_hovering_drawable(self, mouse_x, mouse_y);
    
    
    // Process shared tasks
    switch (GET_TASK(self))
    {
        case (GN_TASK_MOVE_VIEW):
            float pix_x = mouse_x, pix_y = mouse_y;
            gn_canvas_unit_to_pixel(self, &pix_x, &pix_y);
            
            float dx = pix_x - self->task_data.move_view.pixel_start_x;
            float dy = pix_y - self->task_data.move_view.pixel_start_y;
            
            dx /= self->scale;
            dy /= self->scale;
            
            self->translation_x = self->task_data.move_view.trans_start_x - dx;
            self->translation_y = self->task_data.move_view.trans_start_y - dy;
            
            
            // Redraw
            gtk_widget_queue_draw(GTK_WIDGET(self));
            
            break;
    }
    
    
    // Call tab routine
    switch (gn_gui_controller_get_tab(self->gui_controller))
    {
        case (GN_GUI_TAB_START):
            ctrl_on_motion_tab_start(self, mouse_x, mouse_y);
            break;
        case (GN_GUI_TAB_INSERT):
            ctrl_on_motion_tab_insert(self, mouse_x, mouse_y);
            break;
        case (GN_GUI_TAB_DRAW):
            ctrl_on_motion_tab_draw(self, mouse_x, mouse_y);
            gtk_widget_queue_draw(GTK_WIDGET(self));
            break;
        case (GN_GUI_TAB_VIEW):
            ctrl_on_motion_tab_view(self, mouse_x, mouse_y);
            break;
    }
}

static void
ctrl_on_motion_leave(   GtkEventControllerMotion*   motion_controller,
                        gpointer                    user_data)
{
    
}

static void
ctrl_on_motion_tab_start(   GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y)
{
    // Process tasks
    GnDrawable* drawable = self->focused_drawable;
    switch (GET_TASK(self))
    {
        case (GN_TASK_DRAWABLE_DRAG):
            // Is dragging "active"?
            if (!self->task_data.drag.drag_started)
            {
                float phx = self->task_data.drag.drag_start_x;
                float phy = self->task_data.drag.drag_start_y;
                
                if (common_dist(phx, phy, mouse_x, mouse_y) > 3)
                    self->task_data.drag.drag_started = TRUE;
            }
            
            // Drag
            if (self->task_data.drag.drag_started)
            {
                float prev_x = gn_drawable_get_x(drawable);
                float prev_y = gn_drawable_get_y(drawable);
                
                gn_drawable_set_x(drawable, mouse_x - self->task_data.drag.frame_delta_x);
                gn_drawable_set_y(drawable, mouse_y - self->task_data.drag.frame_delta_y);
                
                GN_DRAWABLE_GET_CLASS(drawable)->on_frame_move(drawable, prev_x, prev_y, gn_drawable_get_x(drawable), gn_drawable_get_y(drawable));
            }
            
            // Redraw
            gtk_widget_queue_draw(GTK_WIDGET(self));
            
            break;
        case (GN_TASK_DRAWABLE_RESIZE):
            // Is resize "active"?
            if (!self->task_data.resize.resize_started)
            {
                if (common_dist(self->task_data.resize.drag_start_x, 0, mouse_x, 0) > 3)
                    self->task_data.resize.resize_started = TRUE;
            }
            
            // Resize frame
            if (self->task_data.resize.resize_started)
            {
                float prev_width = gn_drawable_get_width(drawable);
                
                float new_width = common_clamp(mouse_x - gn_drawable_get_x(drawable) + self->task_data.resize.right_delta_x, 25.0f, 9999.0f);
                gn_drawable_set_width(drawable, new_width);
                
                GN_DRAWABLE_GET_CLASS(drawable)->on_frame_move(drawable, prev_width, gn_drawable_get_height(drawable), new_width, gn_drawable_get_height(drawable));
            }
            
            // Redraw
            gtk_widget_queue_draw(GTK_WIDGET(self));
            
            break;
    }
}

static void
ctrl_on_motion_tab_insert(  GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y)
{
    
}

static void
ctrl_on_motion_tab_draw(    GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y)
{
    switch (GET_TASK(self))
    {
        case (GN_TASK_DRAW):
            GnDrawableLine* line = self->task_data.draw.line;
            gn_drawable_line_append_point(line, mouse_x, mouse_y);
            
            break;
        case (GN_TASK_DRAW_ERASER):
            // Process previous motion events first
            float draw_width = gn_gui_controller_get_draw_width(self->gui_controller);
            while (common_dist(mouse_x, mouse_y, self->task_data.draw_motion.prev_mouse_x, self->task_data.draw_motion.prev_mouse_y) > draw_width)
            {
                float dx = mouse_x - self->task_data.draw_motion.prev_mouse_x;
                float dy = mouse_y - self->task_data.draw_motion.prev_mouse_y;
                common_normalize(&dx, &dy);
                
                ctrl_on_motion_tab_draw(    self,
                                            self->task_data.draw_motion.prev_mouse_x + dx * draw_width * 0.5f,
                                            self->task_data.draw_motion.prev_mouse_y + dy * draw_width * 0.5f);
            }
            
            // Update prev mouse
            self->task_data.draw_motion.prev_mouse_x = mouse_x;
            self->task_data.draw_motion.prev_mouse_y = mouse_y;
            
            
            bool found = FALSE;
            
            do
            {
                found = FALSE;
                for (int i = 0; i < g_list_length(self->drawable_list); i++)
                {
                    GnDrawable* drawable = (GnDrawable*)g_list_nth_data(self->drawable_list, i);
                    if (GN_IS_DRAWABLE_LINE(drawable) && gn_drawable_line_contains_circle(GN_DRAWABLE_LINE(drawable), mouse_x, mouse_y, draw_width / 2))
                    {
                        gn_canvas_remove_drawable(self, drawable);
                        g_object_unref(drawable);
                        found = TRUE;
                        
                        break;
                    }
                }
            }
            while (found);
            
            break;
        case (GN_TASK_DRAW_SELECTION):
            self->task_data.draw_selection.to_x = mouse_x;
            self->task_data.draw_selection.to_y = mouse_y;
            
            break;
    }
}

static void
ctrl_on_motion_tab_view(    GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y)
{
    
}


static gboolean
ctrl_on_legacy( GtkEventControllerLegacy*   legacy_controller,
                GdkEvent*                   event,
                gpointer                    user_data)
{
    GnCanvas* self = (GnCanvas*)user_data;
    
    
    // Get mouse position in units
    gdouble mouse_x = 0, mouse_y = 0;
    if (gdk_event_get_position(event, &mouse_x, &mouse_y))
    {
        // Get surface translation
        double trans_x, trans_y;
        gtk_native_get_surface_transform(gtk_widget_get_native(GTK_WIDGET(self)), &trans_x, &trans_y);
        
        
        // Get toplevel widget
        GtkWidget* parent = GTK_WIDGET(self);
        while ((parent = gtk_widget_get_parent(parent)) != NULL && !GTK_IS_WINDOW(parent));
        
        
        // Transform coords
        if (parent)
            gtk_widget_translate_coordinates(parent, GTK_WIDGET(self), mouse_x - trans_x, mouse_y - trans_y, &mouse_x, &mouse_y);
        
        
        // Save pixel position
        self->mouse_pixel_x = mouse_x;
        self->mouse_pixel_y = mouse_y;
        
        
        // Add translation & scale
        float phx = mouse_x, phy = mouse_y;
        gn_canvas_pixel_to_unit(self, &phx, &phy);
        mouse_x = phx;
        mouse_y = phy;
    }
    
    // Start/end shared tasks
    if (!HAS_TASK(self))
    {
        // Start tasks
        
        // View move task
        if (gdk_event_get_event_type(event) == GDK_BUTTON_PRESS && gdk_button_event_get_button(event) == GDK_BUTTON_MIDDLE)
        {
            // Start view move task
            SET_TASK(self, GN_TASK_MOVE_VIEW)
            
            self->task_data.move_view.trans_start_x = self->translation_x;
            self->task_data.move_view.trans_start_y = self->translation_y;
            self->task_data.move_view.pixel_start_x = mouse_x;
            self->task_data.move_view.pixel_start_y = mouse_y;
            
            gn_canvas_unit_to_pixel(self, &self->task_data.move_view.pixel_start_x, &self->task_data.move_view.pixel_start_y);
        }
        else if (gdk_event_get_event_type(event) == GDK_SCROLL && (gdk_event_get_modifier_state(event) & GDK_CONTROL_MASK))
        {
            // Zoom in/out
            // (This isnt really a task as it finishes right away)
            
            // Get scroll dir
            int dir = 0;
            if (gdk_scroll_event_get_direction(event) == GDK_SCROLL_UP)
                dir = 1;
            else if (gdk_scroll_event_get_direction(event) == GDK_SCROLL_DOWN)
                dir = -1;
            
            // Get current mouse pos
            float mx = self->mouse_pixel_x, my = self->mouse_pixel_y;
            gn_canvas_pixel_to_unit(self, &mx, &my);
            
            // Update zoom
            self->scale = common_clamp(self->scale * (1.0f + 0.2f * dir), MIN_ZOOM, MAX_ZOOM);
            
            // Update translation
            float new_mouse_x = self->mouse_pixel_x, new_mouse_y = self->mouse_pixel_y;
            gn_canvas_pixel_to_unit(self, &new_mouse_x, &new_mouse_y); // ..get unit coords after zoom
            
            self->translation_x -= (new_mouse_x - mx);
            self->translation_y -= (new_mouse_y - my);
            
            // Redraw
            gtk_widget_queue_draw(GTK_WIDGET(self));
        }
    }
    else
    {
        // End tasks
        
        if (gdk_event_get_event_type(event) == GDK_BUTTON_RELEASE)
        {
            switch (GET_TASK(self))
            {
                case (GN_TASK_MOVE_VIEW):
                    if (gdk_button_event_get_button(event) == GDK_BUTTON_MIDDLE)
                        END_TASK(self)
                    break;
            }
        }
    }
    
    
    // Call tab routine
    switch (gn_gui_controller_get_tab(self->gui_controller))
    {
        case (GN_GUI_TAB_START):
            ctrl_on_legacy_tab_start(self, event, mouse_x, mouse_y);
            break;
        case (GN_GUI_TAB_INSERT):
            ctrl_on_legacy_tab_insert(self, event, mouse_x, mouse_y);
            break;
        case (GN_GUI_TAB_DRAW):
            ctrl_on_legacy_tab_draw(self, event, mouse_x, mouse_y);
            break;
        case (GN_GUI_TAB_VIEW):
            ctrl_on_legacy_tab_view(self, event, mouse_x, mouse_y);
            break;
    }
    
    
    // Only propagate if there is no task and no focused drawable
    return HAS_TASK(self) || self->focused_drawable == NULL;
}

static void
ctrl_on_legacy_tab_start(   GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y)
{
    if (!HAS_TASK(self))
    {
        // Update currently focused drawable
        if (gdk_event_get_event_type(event) == GDK_BUTTON_PRESS && gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY)
        {
            if (self->hovering_drawable != NULL)
            {
                // Switch focused drawable
                if (self->hovering_drawable != self->focused_drawable)
                    drawable_take_focus(self, self->hovering_drawable, mouse_x, mouse_y);
            }
            else
            {
                // Create text field if nothing is selected
                if (!self->focused_drawable)
                {
                    // Create new text field
                    GnDrawableText* text_field = gn_drawable_text_new();
                    gn_drawable_set_x(GN_DRAWABLE(text_field), mouse_x);
                    gn_drawable_set_y(GN_DRAWABLE(text_field), mouse_y);
                    gn_canvas_append_drawable(self, GN_DRAWABLE(text_field));
                    
                    // Focus this new drawable
                    drawable_take_focus(self, GN_DRAWABLE(text_field), mouse_x, mouse_y);
                }
                else
                {
                    drawable_take_focus(self, NULL, mouse_x, mouse_y);
                }
            }
        }
        
        
        // Drag or resize task
        if (gdk_event_get_event_type(event) == GDK_BUTTON_PRESS && gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY && self->focused_drawable)
        {
            GnDrawable* drawable = self->focused_drawable;
            bool mouse_on_title = FALSE;
            
            if (gn_drawable_contains_point(drawable, mouse_x, mouse_y, &mouse_on_title) && mouse_on_title)
            {
                // Resize or drag?
                if (!gn_drawable_get_is_resizable(drawable) || mouse_x < (gn_drawable_get_x(drawable) + gn_drawable_get_width(drawable) - GN_DRAWABLE_FRAME_TITLE_HEIGHT))
                {
                    // Drag
                    SET_TASK(self, GN_TASK_DRAWABLE_DRAG)
                    
                    self->task_data.drag.drag_started = FALSE;
                    self->task_data.drag.drag_start_x = mouse_x;
                    self->task_data.drag.drag_start_y = mouse_y;
                    self->task_data.drag.frame_delta_x = mouse_x - gn_drawable_get_x(self->focused_drawable);
                    self->task_data.drag.frame_delta_y = mouse_y - gn_drawable_get_y(self->focused_drawable);
                }
                else
                {
                    // Resize
                    SET_TASK(self, GN_TASK_DRAWABLE_RESIZE)
                    
                    self->task_data.resize.resize_started = FALSE;
                    self->task_data.resize.drag_start_x = mouse_x;
                    self->task_data.resize.right_delta_x = gn_drawable_get_x(drawable) + gn_drawable_get_width(drawable) - mouse_x;
                }
            }
        }
    }
    else
    {
        // Try to end task
        
        if (gdk_event_get_event_type(event) == GDK_BUTTON_RELEASE)
        {
            switch (GET_TASK(self))
            {
                case (GN_TASK_DRAWABLE_DRAG):
                case (GN_TASK_DRAWABLE_DRAG_ROUNDED):
                case (GN_TASK_DRAWABLE_RESIZE):
                case (GN_TASK_DRAWABLE_RESIZE_ROUNDED):
                    if (gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY)
                        END_TASK(self)
                    break;
            }
        }
    }
}

static void
ctrl_on_legacy_tab_insert(  GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y)
{
    
}

static void
ctrl_on_legacy_tab_draw(    GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y)
{
    if (!HAS_TASK(self))
    {
        // Start task
        
        // Start drawing line
        if (gdk_event_get_event_type(event) == GDK_BUTTON_PRESS && gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY)
        {
            enum GnDrawTool draw_tool = gn_gui_controller_get_draw_tool(self->gui_controller);
            switch (draw_tool)
            {
                case (GN_DRAW_TOOL_PEN):
                case (GN_DRAW_TOOL_HIGHLIGHTER):
                    SET_TASK(self, GN_TASK_DRAW)
                    
                    GnDrawableLine* line = gn_drawable_line_new();
                    GdkRGBA* color = gn_gui_controller_get_draw_color(self->gui_controller);
                    gn_drawable_line_set_color(line, color->red, color->green, color->blue, color->alpha * (draw_tool == GN_DRAW_TOOL_HIGHLIGHTER ? 0.5f : 1.0f));
                    gn_drawable_line_set_line_width(line, gn_gui_controller_get_draw_width(self->gui_controller));
                    gn_drawable_line_append_point(line, mouse_x, mouse_y);
                    
                    self->task_data.draw.line = line;
                    
                    gn_canvas_append_drawable(self, GN_DRAWABLE(line));
                    
                    gtk_widget_queue_draw(GTK_WIDGET(self));
                    break;
                case (GN_DRAW_TOOL_SELECTION):
                    SET_TASK(self, GN_TASK_DRAW_SELECTION)
                    
                    self->task_data.draw_selection.from_x = mouse_x;
                    self->task_data.draw_selection.from_y = mouse_y;
                    self->task_data.draw_selection.to_x = mouse_x;
                    self->task_data.draw_selection.to_y = mouse_y;
                    
                    break;
                case (GN_DRAW_TOOL_ERASER):
                    SET_TASK(self, GN_TASK_DRAW_ERASER)
                    
                    self->task_data.draw_motion.prev_mouse_x = mouse_x;
                    self->task_data.draw_motion.prev_mouse_y = mouse_y;
                    
                    gtk_widget_queue_draw(GTK_WIDGET(self));
                    
                    break;
            }
        }
    }
    else
    {
        // End task
        
        switch (GET_TASK(self))
        {
            case (GN_TASK_DRAW):
            case (GN_TASK_DRAW_ROUNDED):
                if (gdk_event_get_event_type(event) == GDK_BUTTON_RELEASE && gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY)
                {
                    GnDrawableLine* line = self->task_data.draw.line;
                    self->task_data.draw.line = NULL;
                    
                    gn_drawable_line_end(line);
                    gtk_widget_queue_draw(GTK_WIDGET(self));
                    
                    END_TASK(self)
                }
                
                break;
            case (GN_TASK_DRAW_ERASER):
                if (gdk_event_get_event_type(event) == GDK_BUTTON_RELEASE && gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY)
                {
                    END_TASK(self)
                    gtk_widget_queue_draw(GTK_WIDGET(self));
                }
                break;
            case (GN_TASK_DRAW_SELECTION):
                if (gdk_event_get_event_type(event) == GDK_BUTTON_RELEASE && gdk_button_event_get_button(event) == GDK_BUTTON_PRIMARY)
                {
                    // Get from & to
                    float from_x = self->task_data.draw_selection.from_x, from_y = self->task_data.draw_selection.from_y;
                    float to_x = self->task_data.draw_selection.to_x, to_y = self->task_data.draw_selection.to_y;
                    
                    // ..change values to avoid negative size
                    if (from_x > to_x)
                    {
                        float ph = from_x;
                        from_x = to_x;
                        to_x = ph;
                    }
                    if (from_y > to_y)
                    {
                        float ph = from_y;
                        from_y = to_y;
                        to_y = ph;
                    }
                    
                    
                    // Create selection drawable
                    GnDrawableSelection* sel = NULL;
                    
                    // For each drawable...
                    for (int k = 0; k < g_list_length(self->drawable_list); k++)
                    {
                        GnDrawable* drawable = (GnDrawable*)g_list_nth_data(self->drawable_list, k);
                        
                        if (!GN_IS_DRAWABLE_LINE(drawable))
                            continue;
                        
                        float rx = gn_drawable_get_x(drawable);
                        float ry = gn_drawable_get_y(drawable);
                        float rw = gn_drawable_get_width(drawable);
                        float rh = gn_drawable_get_height(drawable);
                        
                        // Add to list if it overlaps the selection
                        if (common_rect_overlap_rect(rx, ry, rw, rh, from_x, from_y, to_x - from_x, to_y - from_y))
                        {
                            if (!sel)
                                sel = gn_drawable_selection_new();
                            
                            gn_drawable_selection_add_drawable(sel, drawable);
                        }
                    }
                    
                    
                    // End task
                    END_TASK(self)
                    
                    
                    if (sel != NULL)
                    {
                        // Append selection
                        gn_canvas_append_drawable(self, GN_DRAWABLE(sel));
                        drawable_take_focus(self, GN_DRAWABLE(sel), mouse_x, mouse_y);
                        
                        // Change tab & focus
                        gn_gui_controller_set_tab(self->gui_controller, GN_GUI_TAB_START);
                    }
                    
                    // Redraw
                    gtk_widget_queue_draw(GTK_WIDGET(self));
                }
                
                break;
        }
    }
}

static void
ctrl_on_legacy_tab_view(    GnCanvas*   self,
                            GdkEvent*   event,
                            float       mouse_x,
                            float       mouse_y)
{
    
}



// GnCanvas private funcs
static void
update_hovering_drawable(   GnCanvas*   self,
                            float       mouse_x,
                            float       mouse_y)
{
    GnDrawable* prev_drawable = self->hovering_drawable;
    self->hovering_drawable = NULL;
    
    GList* drawable_list = self->drawable_list;
    
    for (int i = 0; i < g_list_length(drawable_list); i++)
    {
        GnDrawable* drawable = (GnDrawable*)g_list_nth_data(drawable_list, i);
        
        if (gn_drawable_get_is_focusable(drawable) && gn_drawable_contains_point(drawable, mouse_x, mouse_y, NULL))
        {
            self->hovering_drawable = drawable;
            
            break;
        }
    }
    
    if (prev_drawable != self->hovering_drawable)
        gtk_widget_queue_draw(GTK_WIDGET(self));
}


static void
drawable_take_focus(    GnCanvas*   self,
                        GnDrawable* drawable,
                        float       mouse_x,
                        float       mouse_y)
{
    // Un-focus previous drawable
    if (self->focused_drawable != NULL)
    {
        GN_DRAWABLE_GET_CLASS(self->focused_drawable)->on_frame_focus_exit(self->focused_drawable, mouse_x, mouse_y);
        
        if (GN_IS_DRAWABLE_TEXT(self->focused_drawable))
        {
            // Delete drawable if its empty
            GtkTextView* tv = gn_drawable_text_get_text_view(GN_DRAWABLE_TEXT(self->focused_drawable));
            GtkTextBuffer* buf = gtk_text_view_get_buffer(tv);
            
            if (gtk_text_buffer_get_char_count(buf) == 0)
            {
                gn_canvas_remove_drawable(self, self->focused_drawable);
                g_object_unref(self->focused_drawable);
            }
        }
        else if (GN_IS_DRAWABLE_SELECTION(self->focused_drawable))
        {
            // Delete drawable if it was just a temporary selection
            gn_canvas_remove_drawable(self, self->focused_drawable);
            g_object_unref(self->focused_drawable);
            
            // Switch tab back to drawing
            drawable = NULL;
            self->hovering_drawable = drawable; // ..we have to set hovering&focused drawable now, as ..._set_tab will trigger an event that works with these two
            self->focused_drawable = drawable;
            gn_gui_controller_set_tab(self->gui_controller, GN_GUI_TAB_DRAW);
        }
    }
    
    // Set new focused drawable
    self->hovering_drawable = drawable;
    self->focused_drawable = drawable;
    
    // Queue Redraw
    gtk_widget_queue_draw(GTK_WIDGET(self));
    
    if (drawable == NULL)
        return;
    
    // Re-add into list so that it will be drawn on top of everything else
    self->drawable_list = g_list_remove(self->drawable_list, self->focused_drawable);
    self->drawable_list = g_list_prepend(self->drawable_list, self->focused_drawable);
    
    // Notify drawable about focus
    GN_DRAWABLE_GET_CLASS(self->focused_drawable)->on_frame_focus_enter(self->focused_drawable, mouse_x, mouse_y);
    
    // Focus GtkWidget if its a text
    if (GN_IS_DRAWABLE_TEXT(drawable))
        gtk_widget_grab_focus(GTK_WIDGET(gn_drawable_text_get_text_view(GN_DRAWABLE_TEXT(drawable))));
}


static void
gui_on_tab_changed( GnGuiController*    gui_controller,
                    enum GnGuiTab       prev_tab_id,
                    enum GnGuiTab       tab_id,
                    gpointer            user_data)
{
    GnCanvas* self = (GnCanvas*)user_data;
    
    // Finish all tasks/activities from the previous tab
    switch (prev_tab_id)
    {
        case (GN_GUI_TAB_START):
            if (HAS_TASK(self))
                END_TASK(self);
            
            drawable_take_focus(self, NULL, 0, 0);
            
            break;
        case (GN_GUI_TAB_INSERT):
            break;
        case (GN_GUI_TAB_DRAW):
            break;
        case (GN_GUI_TAB_VIEW):
            break;
    }
    
    
    // Do preparations for the new tab
    switch (tab_id)
    {
        case (GN_GUI_TAB_START):
            break;
        case (GN_GUI_TAB_INSERT):
            break;
        case (GN_GUI_TAB_DRAW):
            break;
        case (GN_GUI_TAB_VIEW):
            break;
    }
    
    
    // Redraw
    gtk_widget_queue_draw(GTK_WIDGET(self));
}


static void
gui_on_text_bold_toggled(   GnGuiController*    gui_controller,
                            bool                state,
                            gpointer            user_data)
{
    GnCanvas* self = (GnCanvas*)user_data;
    
    if (self->focused_drawable)
        GN_DRAWABLE_GET_CLASS(self->focused_drawable)->on_text_bold_toggle(self->focused_drawable, state);
    else
        gn_gui_controller_tab_start_set_bold_state(gui_controller, FALSE);
}

static void
gui_on_text_italics_toggled(    GnGuiController*    gui_controller,
                                bool                state,
                                gpointer            user_data)
{
    GnCanvas* self = (GnCanvas*)user_data;
    
    if (self->focused_drawable)
        GN_DRAWABLE_GET_CLASS(self->focused_drawable)->on_text_italic_toggle(self->focused_drawable, state);
    else
        gn_gui_controller_tab_start_set_italic_state(gui_controller, FALSE);
}

static void
gui_on_text_underlined_toggled( GnGuiController*    gui_controller,
                                bool                state,
                                gpointer            user_data)

{
    GnCanvas* self = (GnCanvas*)user_data;
    
    if (self->focused_drawable)
        GN_DRAWABLE_GET_CLASS(self->focused_drawable)->on_text_underline_toggle(self->focused_drawable, state);
    else
        gn_gui_controller_tab_start_set_underline_state(gui_controller, FALSE);
}




// GET/SET FUNCTIONS //

PUBLIC_GET_SET(GnCanvas, gn_canvas, translation_x, float)
PUBLIC_GET_SET(GnCanvas, gn_canvas, translation_y, float)
PUBLIC_GET_SET(GnCanvas, gn_canvas, scale, float)

PUBLIC_GETTER(GnCanvas, gn_canvas, hovering_drawable, GnDrawable*)
PUBLIC_GETTER(GnCanvas, gn_canvas, focused_drawable, GnDrawable*)