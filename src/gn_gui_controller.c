#include "gn_gui_controller.h"
#include "gn_canvas.h"
#include "gn_color_indicator.h"
#include "gn_width_indicator.h"


// MACROS //
#define DRAW_COLOR_BUTTON_COUNT 5
#define DRAW_WIDTH_BUTTON_COUNT 3


// STRUCTS //
typedef struct
{
    int id;
    int page_num;
} GnToolTab;

typedef struct
{
    GnToolTab parent;
    
    GtkBox* box;
        GtkFontButton* btn_font;
        GtkBox* font_box;
            GtkToggleButton* btn_bold;
            GtkToggleButton* btn_italics;
            GtkToggleButton* btn_underlined;
} GnToolTabStart;

typedef struct
{
    GnToolTab parent;
    
    GtkWidget* testwidget;
} GnToolTabInsert;

typedef struct
{
    GnToolTab parent;
    
    GtkBox* box;
        GtkBox* tool_box;
            GtkToggleButton* btn_tool[4];
        GtkBox* color_box;
            GtkToggleButton* btn_color[DRAW_COLOR_BUTTON_COUNT];
            GnColorIndicator* color_indic[DRAW_COLOR_BUTTON_COUNT];
        GtkBox* width_box;
            GtkToggleButton* btn_width[DRAW_WIDTH_BUTTON_COUNT];
            GnWidthIndicator* width_indic[DRAW_WIDTH_BUTTON_COUNT];
} GnToolTabDraw;

typedef struct
{
    GnToolTab parent;
    
    GtkWidget* testwidget;
} GnToolTabView;



// PROTOTYPES //
static void
gn_gui_controller_dispose(GObject* self_object);

static void
gn_gui_controller_finalize(GObject* self_object);

static void
gn_gui_controller_add_tab(  GnGuiController*    self,
                            int                 id);

static void
on_switch_page( GtkNotebook*    notebook,
                GtkWidget*      page,
                guint           page_num,
                gpointer        user_data);

static void
on_text_bold_toggled(   GtkToggleButton*    btn,
                        gpointer            user_data);

static void
on_text_italics_toggled(    GtkToggleButton*    btn,
                            gpointer            user_data);

static void
on_text_underlined_toggled( GtkToggleButton*    btn,
                            gpointer            user_data);

static GtkWidget*
create_tab_start(GnToolTab** tool_tab);

static GtkWidget*
create_tab_insert(GnToolTab** tool_tab);

static GtkWidget*
create_tab_draw(GnToolTab** tool_tab);

static GtkWidget*
create_tab_view(GnToolTab** tool_tab);

static GnToolTab*
get_tool_tab(   GnGuiController*    self, 
                enum GnGuiTab       tab_id);


// OBJECT CONSTRUCTION //
struct _GnGuiController
{
    GObject parent;
    
    GtkApplication* app;
    AdwApplicationWindow* window;
    
    enum GnGuiTab prev_tab;
    
    GtkBox* toplevel_box;
        AdwHeaderBar* header_bar;
        GtkNotebook* tools_notebook;
            // ..tabs here
            GSList* tool_tab_list; // ..stores page numbers and their corresponding tab ID's
        GnCanvas* canvas;
};
G_DEFINE_TYPE(GnGuiController, gn_gui_controller, G_TYPE_OBJECT)

static void
gn_gui_controller_class_init(GnGuiControllerClass* klass)
{
    ((GObjectClass*)klass)->dispose = gn_gui_controller_dispose;
    ((GObjectClass*)klass)->finalize = gn_gui_controller_finalize;
    
    g_signal_new(   "tab-changed",
                    GN_TYPE_GUI_CONTROLLER,
                    G_SIGNAL_RUN_FIRST,
                    0,
                    NULL,
                    NULL,
                    NULL,
                    G_TYPE_NONE,
                    2,
                    G_TYPE_INT,
                    G_TYPE_INT);
    
    g_signal_new("toggle-text-bold", GN_TYPE_GUI_CONTROLLER, G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
    g_signal_new("toggle-text-underline", GN_TYPE_GUI_CONTROLLER, G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
    g_signal_new("toggle-text-italic", GN_TYPE_GUI_CONTROLLER, G_SIGNAL_RUN_FIRST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}

static void
gn_gui_controller_init(GnGuiController* self)
{
    self->prev_tab = GN_GUI_TAB_START;
}


// OBJECT DECONSTRUCTION //
static void
gn_gui_controller_dispose(GObject* self_object)
{
    // TODO: Unref all references you own of other GObjects
    
    G_OBJECT_CLASS(gn_gui_controller_parent_class)->dispose(self_object);
}

static void
gn_gui_controller_finalize(GObject* self_object)
{
    // TODO: Free any memory you allocated
    
    G_OBJECT_CLASS(gn_gui_controller_parent_class)->finalize(self_object);
}


// PUBLIC FUNCTIONS //

GnGuiController*
gn_gui_controller_new(GtkApplication* app)
{
    GnGuiController* self = (GnGuiController*)g_object_new(GN_TYPE_GUI_CONTROLLER, NULL);
    self->app = app;
    
    // Create window
    self->window = ADW_APPLICATION_WINDOW(adw_application_window_new(app));
    gtk_window_set_title(GTK_WINDOW(self->window), "GNotes");
    gtk_window_set_default_size(GTK_WINDOW(self->window), 800, 600);
    
    
    // Create window content
    self->toplevel_box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    
    {
        self->header_bar = ADW_HEADER_BAR(adw_header_bar_new());
        
        self->tools_notebook = GTK_NOTEBOOK(gtk_notebook_new());
        
        {
            gn_gui_controller_add_tab(self, GN_GUI_TAB_START);
            gn_gui_controller_add_tab(self, GN_GUI_TAB_INSERT);
            gn_gui_controller_add_tab(self, GN_GUI_TAB_DRAW);
            gn_gui_controller_add_tab(self, GN_GUI_TAB_VIEW);
        }
        
        self->canvas = gn_canvas_new(self);
        
        gtk_box_append(self->toplevel_box, GTK_WIDGET(self->header_bar));
        gtk_box_append(self->toplevel_box, GTK_WIDGET(self->tools_notebook));
        gtk_box_append(self->toplevel_box, GTK_WIDGET(self->canvas)); // TODO: there will be multiple canvases later
    }
    
    
    // Connect signals
    g_signal_connect(self->tools_notebook, "switch-page", G_CALLBACK(on_switch_page), self);
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    if (tab_start)
    {
        g_signal_connect(tab_start->btn_bold, "toggled", G_CALLBACK(on_text_bold_toggled), self);
        g_signal_connect(tab_start->btn_italics, "toggled", G_CALLBACK(on_text_italics_toggled), self);
        g_signal_connect(tab_start->btn_underlined, "toggled", G_CALLBACK(on_text_underlined_toggled), self);
    }
    
    
    // Set window content
    adw_application_window_set_content(self->window, GTK_WIDGET(self->toplevel_box));
    
    
    // Show window
    gtk_widget_show((GtkWidget*)self->window);
    
    return self;
}


bool
gn_gui_controller_is_tab(	GnGuiController*	self,
							enum GnGuiTab		id)
{
    // Get current tab struct
    GnToolTab* tab = NULL;
    
    GSList* list = self->tool_tab_list;
    while (list != NULL)
    {
        GnToolTab* ph = list->data;
        if (ph->id == id)
        {
            tab = ph;
            break;
        }
        
        // Next
        list = list->next;
    }
    
    if (!tab)
        return false;
    
    
    return gtk_notebook_get_current_page(self->tools_notebook) == tab->page_num;
}

enum GnGuiTab
gn_gui_controller_get_tab(GnGuiController* self)
{
    return gn_gui_controller_get_tab_at_page(self, gtk_notebook_get_current_page(self->tools_notebook));
}

enum GnGuiTab
gn_gui_controller_get_tab_at_page(  GnGuiController*    self,
                                    int                 page_num)
{
    // Get current tab struct
    GnToolTab* tab = NULL;
    
    GSList* list = self->tool_tab_list;
    while (list != NULL)
    {
        GnToolTab* ph = list->data;
        if (ph->page_num == page_num)
        {
            tab = ph;
            break;
        }
        
        // Next
        list = list->next;
    }
    
    return tab->id;
}

void
gn_gui_controller_set_tab(	GnGuiController*	self,
							enum GnGuiTab		id)
{
    // Get current tab struct
    GnToolTab* tab = NULL;
    
    GSList* list = self->tool_tab_list;
    while (list != NULL)
    {
        GnToolTab* ph = list->data;
        if (ph->id == id)
        {
            tab = ph;
            break;
        }
        
        // Next
        list = list->next;
    }
    
    if (!tab)
        return;
    
    
    // Set page
    gtk_notebook_set_current_page(self->tools_notebook, tab->page_num);
}


GdkRGBA*
gn_gui_controller_get_draw_color(GnGuiController* self)
{
    static GdkRGBA fallback_color = (GdkRGBA){ 1.0f, 0.0f, 0.0f, 1.0f };
    
    GSList* list = self->tool_tab_list;
    while (list != NULL)
    {
        GnToolTab* tab = (GnToolTab*)list->data;
        
        if (tab->id == GN_GUI_TAB_DRAW)
        {
            GnToolTabDraw* ph = (GnToolTabDraw*)tab;
            
            for (int i = 0; i < DRAW_COLOR_BUTTON_COUNT; i++)
            {
                if (gtk_toggle_button_get_active(ph->btn_color[i]))
                    return gn_color_indicator_get_color(ph->color_indic[i]);
            }
        }
        
        // Next
        list = list->next;
    }
    
    return &fallback_color;
}

float
gn_gui_controller_get_draw_width(GnGuiController* self)
{
    static float fallback_width = 1.0f;
    
    float width_mult = 1.0f;
    switch (gn_gui_controller_get_draw_tool(self))
    {
        case (GN_DRAW_TOOL_HIGHLIGHTER): width_mult = 8.0f; break;
        case (GN_DRAW_TOOL_ERASER): width_mult = 4.0f; break;
    }
    
    GSList* list = self->tool_tab_list;
    while (list != NULL)
    {
        GnToolTab* tab = (GnToolTab*)list->data;
        
        if (tab->id == GN_GUI_TAB_DRAW)
        {
            GnToolTabDraw* ph = (GnToolTabDraw*)tab;
            
            for (int i = 0; i < DRAW_WIDTH_BUTTON_COUNT; i++)
            {
                if (gtk_toggle_button_get_active(ph->btn_width[i]))
                    return gn_width_indicator_get_value(ph->width_indic[i]) * width_mult;
            }
        }
        
        // Next
        list = list->next;
    }
    
    return fallback_width;
}

enum GnDrawTool
gn_gui_controller_get_draw_tool(GnGuiController* self)
{
    GSList* list = self->tool_tab_list;
    while (list != NULL)
    {
        GnToolTab* tab = (GnToolTab*)list->data;
        
        if (tab->id == GN_GUI_TAB_DRAW)
        {
            GnToolTabDraw* ph = (GnToolTabDraw*)tab;
            
            for (int i = 0; i < 4; i++)
            {
                if (gtk_toggle_button_get_active(ph->btn_tool[i]))
                    return i + 1; // tool tab enum starts counting from 1
            }
        }
        
        // Next
        list = list->next;
    }
     
    return GN_DRAW_TOOL_PEN;
}



void
gn_gui_controller_tab_start_set_bold_state( GnGuiController*    self,
                                            bool                state)
{
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    
    g_signal_handlers_block_by_func(tab_start->btn_bold, &on_text_bold_toggled, self);
    gtk_toggle_button_set_active(tab_start->btn_bold, state);
    g_signal_handlers_unblock_by_func(tab_start->btn_bold, &on_text_bold_toggled, self);
}

bool
gn_gui_controller_tab_start_get_bold_state(GnGuiController* self)
{
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    
    return gtk_toggle_button_get_active(tab_start->btn_bold);
}


void
gn_gui_controller_tab_start_set_italic_state(   GnGuiController*    self,
                                                bool                state)
{
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    
    g_signal_handlers_block_by_func(tab_start->btn_italics, &on_text_italics_toggled, self);
    gtk_toggle_button_set_active(tab_start->btn_italics, state);
    g_signal_handlers_unblock_by_func(tab_start->btn_italics, &on_text_italics_toggled, self);
}

bool
gn_gui_controller_tab_start_get_italic_state(GnGuiController* self)
{
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    
    return gtk_toggle_button_get_active(tab_start->btn_italics);
}


void
gn_gui_controller_tab_start_set_underline_state(    GnGuiController*    self,
                                                    bool                state)
{
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    
    g_signal_handlers_block_by_func(tab_start->btn_underlined, &on_text_underlined_toggled, self);
    gtk_toggle_button_set_active(tab_start->btn_underlined, state);
    g_signal_handlers_unblock_by_func(tab_start->btn_underlined, &on_text_underlined_toggled, self);
}

bool
gn_gui_controller_tab_start_get_underline_state(GnGuiController* self)
{
    GnToolTabStart* tab_start = (GnToolTabStart*)get_tool_tab(self, GN_GUI_TAB_START);
    
    return gtk_toggle_button_get_active(tab_start->btn_underlined);
}



// PRIVATE FUNCTIONS //

static void
gn_gui_controller_add_tab(  GnGuiController*    self,
                            int                 id)
{
    char* title = NULL;
    GtkWidget* child = NULL; // TODO: create child widgets
    
    GnToolTab* new_tab = NULL;
    
    
    switch (id)
    {
        case (GN_GUI_TAB_START):
            title = "Start";
            child = create_tab_start(&new_tab);
            break;
        case (GN_GUI_TAB_INSERT):
            title = "Insert";
            child = create_tab_insert(&new_tab);
            break;
        case (GN_GUI_TAB_DRAW):
            title = "Draw";
            child = create_tab_draw(&new_tab);
            break;
        case (GN_GUI_TAB_VIEW):
            title = "View";
            child = create_tab_view(&new_tab);
            break;
    }
    
    if (!title)
    {
        g_error("Unknown title ID");
        return;
    }
    
    
    new_tab->id = id;
    new_tab->page_num = gtk_notebook_append_page(self->tools_notebook, child, gtk_label_new(title));
    
    self->tool_tab_list = g_slist_append(self->tool_tab_list, new_tab);
}

static GtkWidget*
create_tab_start(GnToolTab** tool_tab)
{
    // Allocate tab data
    *tool_tab = (GnToolTab*)malloc(sizeof(GnToolTabStart));
    GnToolTabStart* tab_start = (GnToolTabStart*)*tool_tab;
    
    tab_start->box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 16));
    gtk_widget_set_margin_start(GTK_WIDGET(tab_start->box), 8);
    gtk_widget_set_margin_end(GTK_WIDGET(tab_start->box), 8);
        tab_start->btn_font = GTK_FONT_BUTTON(gtk_font_button_new_with_font("Cantarell 11"));
        gtk_box_append(tab_start->box, GTK_WIDGET(tab_start->btn_font));
        
        tab_start->font_box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
        gtk_widget_add_css_class(GTK_WIDGET(tab_start->font_box), "linked");
            tab_start->btn_bold = GTK_TOGGLE_BUTTON(gtk_toggle_button_new());
            gtk_button_set_child(GTK_BUTTON(tab_start->btn_bold), gtk_image_new_from_icon_name("format-text-bold-symbolic"));
            
            tab_start->btn_italics = GTK_TOGGLE_BUTTON(gtk_toggle_button_new());
            gtk_button_set_child(GTK_BUTTON(tab_start->btn_italics), gtk_image_new_from_icon_name("format-text-italic-symbolic"));
            
            tab_start->btn_underlined = GTK_TOGGLE_BUTTON(gtk_toggle_button_new());
            gtk_button_set_child(GTK_BUTTON(tab_start->btn_underlined), gtk_image_new_from_icon_name("format-text-underline-symbolic"));
            
            gtk_box_append(tab_start->font_box, GTK_WIDGET(tab_start->btn_bold));
            gtk_box_append(tab_start->font_box, GTK_WIDGET(tab_start->btn_italics));
            gtk_box_append(tab_start->font_box, GTK_WIDGET(tab_start->btn_underlined));
        gtk_box_append(tab_start->box, GTK_WIDGET(tab_start->font_box));
    
    
    // Return child widget
    return GTK_WIDGET(tab_start->box);
}

static GtkWidget*
create_tab_insert(GnToolTab** tool_tab)
{
    // Allocate tab data
    *tool_tab = (GnToolTab*)malloc(sizeof(GnToolTabInsert));
    GnToolTabInsert* tab_insert = (GnToolTabInsert*)*tool_tab;
    
    // ...
    
    // Return child widget
    return gtk_button_new_with_label("test2");
}

static GtkWidget*
create_tab_draw(GnToolTab** tool_tab)
{
    // Allocate tab data
    *tool_tab = (GnToolTab*)malloc(sizeof(GnToolTabDraw));
    GnToolTabDraw* tab_draw = (GnToolTabDraw*)*tool_tab;
    
    
    // Create tab
    tab_draw->box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 16));
    gtk_widget_set_margin_start(GTK_WIDGET(tab_draw->box), 8);
    gtk_widget_set_margin_end(GTK_WIDGET(tab_draw->box), 8);
        tab_draw->tool_box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
        gtk_widget_add_css_class(GTK_WIDGET(tab_draw->tool_box), "linked");
            for (int i = 0; i < 4; i++)
            {
                tab_draw->btn_tool[i] = GTK_TOGGLE_BUTTON(gtk_toggle_button_new());
                
                if (i != 0)
                    gtk_toggle_button_set_group(tab_draw->btn_tool[i], tab_draw->btn_tool[i - 1]);
                
                gtk_box_append(tab_draw->tool_box, GTK_WIDGET(tab_draw->btn_tool[i]));
            }
            gtk_toggle_button_set_active(tab_draw->btn_tool[0], TRUE);
            gtk_button_set_child(GTK_BUTTON(tab_draw->btn_tool[0]), gtk_image_new_from_file("data/icons/pencil-symbolic.svg"));
            gtk_button_set_child(GTK_BUTTON(tab_draw->btn_tool[1]), gtk_image_new_from_file("data/icons/marker-symbolic.svg"));
            gtk_button_set_child(GTK_BUTTON(tab_draw->btn_tool[2]), gtk_image_new_from_file("data/icons/select-symbolic.svg"));
            gtk_button_set_child(GTK_BUTTON(tab_draw->btn_tool[3]), gtk_image_new_from_file("data/icons/eraser2-symbolic.svg"));
        gtk_box_append(tab_draw->box, GTK_WIDGET(tab_draw->tool_box));
        
        tab_draw->color_box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
        gtk_widget_add_css_class(GTK_WIDGET(tab_draw->color_box), "linked");
            for (int i = 0; i < DRAW_COLOR_BUTTON_COUNT; i++)
            {
                tab_draw->btn_color[i] = GTK_TOGGLE_BUTTON(gtk_toggle_button_new());
                tab_draw->color_indic[i] = gn_color_indicator_new(0.0f, 0.0f, 0.0f, 1.0f);
                
                if (i != 0)
                    gtk_toggle_button_set_group(tab_draw->btn_color[i], tab_draw->btn_color[i - 1]);
                
                switch (i)
                {
                    case (0): gn_color_indicator_set_color(tab_draw->color_indic[i], 1.0f, 0.3f, 0.5f, 1.0f); break;
                    case (1): gn_color_indicator_set_color(tab_draw->color_indic[i], 0.3f, 1.0f, 0.3f, 1.0f); break;
                    case (2): gn_color_indicator_set_color(tab_draw->color_indic[i], 0.3f, 0.5f, 1.0f, 1.0f); break;
                    case (3): gn_color_indicator_set_color(tab_draw->color_indic[i], 0.3f, 0.3f, 0.3f, 1.0f); break;
                    case (4): gn_color_indicator_set_color(tab_draw->color_indic[i], 1.0f, 0.5f, 1.0f, 1.0f); break;
                }
                
                gtk_button_set_child(GTK_BUTTON(tab_draw->btn_color[i]), GTK_WIDGET(tab_draw->color_indic[i]));
                gtk_toggle_button_set_active(tab_draw->btn_color[0], TRUE);
                gtk_box_append(tab_draw->color_box, GTK_WIDGET(tab_draw->btn_color[i]));
            }
        gtk_box_append(tab_draw->box, GTK_WIDGET(tab_draw->color_box));
        
        tab_draw->width_box = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
        gtk_widget_add_css_class(GTK_WIDGET(tab_draw->width_box), "linked");
            for (int i = 0; i < DRAW_WIDTH_BUTTON_COUNT; i++)
            {
                tab_draw->btn_width[i] = GTK_TOGGLE_BUTTON(gtk_toggle_button_new());
                tab_draw->width_indic[i] = gn_width_indicator_new(3.0f, 1.0f, 8.0f);
                
                if (i != 0)
                    gtk_toggle_button_set_group(tab_draw->btn_width[i], tab_draw->btn_width[i - 1]);
                
                switch (i)
                {
                    case (0): gn_width_indicator_set_value(tab_draw->width_indic[i], 2.0f); break;
                    case (1): gn_width_indicator_set_value(tab_draw->width_indic[i], 4.0f); break;
                    case (2): gn_width_indicator_set_value(tab_draw->width_indic[i], 8.0f); break;
                }
                
                gtk_button_set_child(GTK_BUTTON(tab_draw->btn_width[i]), GTK_WIDGET(tab_draw->width_indic[i]));
                gtk_toggle_button_set_active(tab_draw->btn_width[0], TRUE);
                gtk_box_append(tab_draw->width_box, GTK_WIDGET(tab_draw->btn_width[i]));
            }
        gtk_box_append(tab_draw->box, GTK_WIDGET(tab_draw->width_box));
    
    
    // Return child widget
    return GTK_WIDGET(tab_draw->box);
}

static GtkWidget*
create_tab_view(GnToolTab** tool_tab)
{
    // Allocate tab data
    *tool_tab = (GnToolTab*)malloc(sizeof(GnToolTabView));
    GnToolTabView* tab_view = (GnToolTabView*)*tool_tab;
    
    // ...
    
    // Return child widget
    return gtk_button_new_with_label("test4");
}

static GnToolTab*
get_tool_tab(   GnGuiController*    self, 
                enum GnGuiTab       tab_id)
{
    GSList* list = self->tool_tab_list;
    
    for (int i = 0; i < g_slist_length(list); i++)
    {
        GnToolTab* tab = (GnToolTab*)g_slist_nth_data(list, i);
        
        if (tab->id == tab_id)
            return tab;
    }
    
    return NULL;
}




static void
on_switch_page( GtkNotebook*    notebook,
                GtkWidget*      page,
                guint           page_num,
                gpointer        user_data)
{
    GnGuiController* self = (GnGuiController*)user_data;
    
    enum GnGuiTab cur_tab = gn_gui_controller_get_tab_at_page(self, page_num);
    
    g_signal_emit_by_name(  self,
                            "tab-changed",
                            self->prev_tab,
                            cur_tab);
    
    self->prev_tab = cur_tab;
}

static void
on_text_bold_toggled(   GtkToggleButton*    btn,
                        gpointer            user_data)
{
    GnGuiController* self = (GnGuiController*)user_data;
    
    g_signal_emit_by_name(self, "toggle-text-bold", gtk_toggle_button_get_active(btn));
}

static void
on_text_italics_toggled(    GtkToggleButton*    btn,
                            gpointer            user_data)
{
    GnGuiController* self = (GnGuiController*)user_data;
    
    g_signal_emit_by_name(self, "toggle-text-italic", gtk_toggle_button_get_active(btn));
}

static void
on_text_underlined_toggled( GtkToggleButton*    btn,
                            gpointer            user_data)
{
    GnGuiController* self = (GnGuiController*)user_data;
    
    g_signal_emit_by_name(self, "toggle-text-underline", gtk_toggle_button_get_active(btn));
}