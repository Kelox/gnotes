#include "gn_width_indicator.h"


// PROTOTYPES //
static void
gn_width_indicator_dispose(GObject* self_object);

static void
gn_width_indicator_finalize(GObject* self_object);

static GtkSizeRequestMode
get_request_mode(GtkWidget* widget);

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline);

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap);



// OBJECT CONSTRUCTION //
struct _GnWidthIndicator
{
    GtkWidget parent;
    
    float value;
    float min_value;
    float max_value;
};
G_DEFINE_TYPE(GnWidthIndicator, gn_width_indicator, GTK_TYPE_WIDGET)

static void
gn_width_indicator_class_init(GnWidthIndicatorClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &gn_width_indicator_dispose;
    G_OBJECT_CLASS(klass)->finalize = &gn_width_indicator_finalize;
    
    GTK_WIDGET_CLASS(klass)->get_request_mode = &get_request_mode;
    GTK_WIDGET_CLASS(klass)->measure = &measure;
    GTK_WIDGET_CLASS(klass)->snapshot = &snapshot;
}

static void
gn_width_indicator_init(GnWidthIndicator* self)
{
    
}


// OBJECT DECONSTRUCTION //
static void
gn_width_indicator_dispose(GObject* self_object)
{
    G_OBJECT_CLASS(gn_width_indicator_parent_class)->dispose(self_object);
}

static void
gn_width_indicator_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_width_indicator_parent_class)->finalize(self_object);
}



// METHODS //
GnWidthIndicator*
gn_width_indicator_new( float value,
                        float min_value,
                        float max_value)
{
    GnWidthIndicator* self = GN_WIDTH_INDICATOR(g_object_new(GN_TYPE_WIDTH_INDICATOR, NULL));
    
    self->value = value;
    self->min_value = min_value;
    self->max_value = max_value;
    
    return self;
}


float
gn_width_indicator_get_value(GnWidthIndicator* self)
{
    return self->value;
}


void
gn_width_indicator_set_value(   GnWidthIndicator*   self,
                                float               value)
{
    self->value = value;
    
    gtk_widget_queue_draw(GTK_WIDGET(self));
}



// PRIVATE FUNCTIONS //
static GtkSizeRequestMode
get_request_mode(GtkWidget* widget)
{
    return GTK_SIZE_REQUEST_WIDTH_FOR_HEIGHT;
}

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline)
{
    if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
        if (for_size == -1)
        {
            *minimum = 20;
            *natural = 28;
        }
        else
        {
            *natural = for_size + 4;
        }
    }
    else
    {
        if (for_size == -1)
        {
            *minimum = 16;
            *natural = 24;
        }
        else
        {
            *natural = for_size - 4;
        }
    }
}

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap)
{
    GnWidthIndicator* self = GN_WIDTH_INDICATOR(self_widget);
    
    // Get size
    float width = gtk_widget_get_allocated_width(self_widget);
    float height = gtk_widget_get_allocated_height(self_widget);
    
    
    // Draw
    cairo_t* cairo = gtk_snapshot_append_cairo(snap, &GRAPHENE_RECT_INIT(0, 0, width, height));
    
    cairo_set_source_rgb(cairo, 0, 0, 0);
    float m = (self->value - self->min_value) / (self->max_value - self->min_value);
    float line_width = (height / 4.0f) * (2.0f * m + 1.0f);
    cairo_set_line_width(cairo, line_width);
    cairo_set_line_cap(cairo, CAIRO_LINE_CAP_ROUND);
    
    cairo_move_to(cairo, line_width / 2.0f, height / 2.0f);
    cairo_line_to(cairo, width - (line_width / 2.0f), height / 2.0f);
    cairo_stroke(cairo);
    
    // Done
    cairo_destroy(cairo);
}