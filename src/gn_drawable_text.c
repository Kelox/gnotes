#include "gn_drawable_text.h"
#include "common_funcs.h"


// PROTOTYPES //
static void
gn_drawable_text_dispose(GObject* self_object);

static void
gn_drawable_text_finalize(GObject* self_object);

static void
on_canvas_append(GnDrawable* self_drawable);

static void
on_canvas_remove(GnDrawable* self_drawable);

static void
on_snapshot(    GnDrawable*     self_drawable,
                GtkSnapshot*    snap);

static void
on_frame_focus_enter(   GnDrawable*     self_drawable,
                        float           x,
                        float           y);

static void
on_frame_focus_exit(    GnDrawable*     self_drawable,
                        float           x,
                        float           y);

static void
on_text_bold_toggle(    GnDrawable*     self_drawable,
                        bool            state);

static void
on_text_italic_toggle(  GnDrawable*     self_drawable,
                        bool            state);

static void
on_text_underline_toggle(   GnDrawable*     self_drawable,
                            bool            state);

static void
ctrl_on_motion_enter(   GtkEventControllerMotion*   motion_controller,
                        gdouble                     x,
                        gdouble                     y,
                        gpointer                    user_data);

static void
ctrl_on_motion_leave(   GtkEventControllerMotion*   motion_controller,
                        gpointer                    user_data);

static gboolean
frame_tick_callback(    GtkWidget*      txt_widget,
                        GdkFrameClock*  frame_clock,
                        gpointer        user_data);

static void
update_toggle_buttons(GnDrawableText* self);



// OBJECT CONSTRUCTION //
struct _GnDrawableText
{
    GnDrawable parent;
    
    GtkTextView* text_view;
    GtkEventController* motion_controller;
    
    GtkTextTag* tag_bold;
    GtkTextTag* tag_italic;
    GtkTextTag* tag_underline;
    
    int prev_offset_start, prev_offset_end; // ..used during draw event to detect whether the selection changed
};
G_DEFINE_TYPE(GnDrawableText, gn_drawable_text, GN_TYPE_DRAWABLE)

static void
gn_drawable_text_class_init(GnDrawableTextClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &gn_drawable_text_dispose;
    G_OBJECT_CLASS(klass)->finalize = &gn_drawable_text_finalize;
    
    GN_DRAWABLE_CLASS(klass)->on_canvas_append = &on_canvas_append;
    GN_DRAWABLE_CLASS(klass)->on_canvas_remove = &on_canvas_remove;
    GN_DRAWABLE_CLASS(klass)->on_snapshot = &on_snapshot;
    GN_DRAWABLE_CLASS(klass)->on_frame_focus_enter = &on_frame_focus_enter;
    GN_DRAWABLE_CLASS(klass)->on_frame_focus_exit = &on_frame_focus_exit;
    GN_DRAWABLE_CLASS(klass)->on_text_bold_toggle = &on_text_bold_toggle;
    GN_DRAWABLE_CLASS(klass)->on_text_italic_toggle = &on_text_italic_toggle;
    GN_DRAWABLE_CLASS(klass)->on_text_underline_toggle = &on_text_underline_toggle;
}

static void
gn_drawable_text_init(GnDrawableText* self)
{
    self->text_view = NULL;
    
    gn_drawable_set_x(GN_DRAWABLE(self), 100);
    gn_drawable_set_y(GN_DRAWABLE(self), 100);
    
    gn_drawable_set_width(GN_DRAWABLE(self), 100);
    gn_drawable_set_height(GN_DRAWABLE(self), 20);
    
    gn_drawable_set_use_render_node(GN_DRAWABLE(self), FALSE);
    gn_drawable_set_is_focusable(GN_DRAWABLE(self), TRUE);
    gn_drawable_set_is_resizable(GN_DRAWABLE(self), TRUE);
}


// OBJECT DECONSTRUCTION //
static void
gn_drawable_text_dispose(GObject* self_object)
{
    G_OBJECT_CLASS(gn_drawable_text_parent_class)->dispose(self_object);
}

static void
gn_drawable_text_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_drawable_text_parent_class)->finalize(self_object);
}


// METHODS //
GnDrawableText*
gn_drawable_text_new()
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(g_object_new(GN_TYPE_DRAWABLE_TEXT, NULL));
    
    return self;
}

GtkTextView*
gn_drawable_text_get_text_view(GnDrawableText* self)
{
    return self->text_view;
}


// VIRTUAL METHODS //
static void
on_canvas_append(GnDrawable* self_drawable)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    
    // Prepare text view
    self->text_view = GTK_TEXT_VIEW(gtk_text_view_new());
    gtk_widget_set_parent(GTK_WIDGET(self->text_view), GTK_WIDGET(gn_drawable_get_canvas(self_drawable)));
    gtk_widget_set_focusable(GTK_WIDGET(self->text_view), FALSE); // ..only set focusable when canvas tells us that we are
    self->prev_offset_start = -1, self->prev_offset_end = -1;
    
    
    // Create text tags
    GtkTextBuffer* buf = gtk_text_view_get_buffer(self->text_view);
    self->tag_bold = gtk_text_buffer_create_tag(buf, "BOLD-TAG", "weight", PANGO_WEIGHT_BOLD, NULL);
    self->tag_italic = gtk_text_buffer_create_tag(buf, "ITALIC-TAG", "style", PANGO_STYLE_ITALIC, NULL);
    self->tag_underline = gtk_text_buffer_create_tag(buf, "UNDERLINE-TAG", "underline", PANGO_UNDERLINE_SINGLE, NULL);
        
    
    // Events
    self->motion_controller = gtk_event_controller_motion_new();
    g_signal_connect(self->motion_controller, "enter", G_CALLBACK(ctrl_on_motion_enter), self);
    g_signal_connect(self->motion_controller, "leave", G_CALLBACK(ctrl_on_motion_leave), self);
    gtk_widget_add_controller(GTK_WIDGET(self->text_view), self->motion_controller);
    
    gtk_widget_add_tick_callback(GTK_WIDGET(self->text_view), &frame_tick_callback, self, NULL);
}


static void
on_canvas_remove(GnDrawable* self_drawable)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    
    gtk_widget_unparent(GTK_WIDGET(self->text_view));
    self->text_view = NULL;
    
    self->motion_controller = NULL; // TODO: Do I have to free this?
}


static void
on_snapshot(    GnDrawable*     self_drawable,
                GtkSnapshot*    snap)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    
    // Don't do anything if we cannot draw the text view
    if (self->text_view == NULL || gtk_widget_get_parent(GTK_WIDGET(self->text_view)) == NULL)
        return;
    
    
    // Get canvas
    GnCanvas* canvas = gn_drawable_get_canvas(self_drawable);
    
    
    // Get position & size
    float pos_x = gn_drawable_get_x(self_drawable);
    float pos_y = gn_drawable_get_y(self_drawable);
    gn_canvas_unit_to_pixel(canvas, &pos_x, &pos_y);
    
    float width = gn_drawable_get_width(self_drawable);
    float height = gn_drawable_get_height(self_drawable);
    
    float canvas_width = gtk_widget_get_allocated_width(GTK_WIDGET(gn_drawable_get_canvas(self_drawable)));
    float canvas_height = gtk_widget_get_allocated_height(GTK_WIDGET(gn_drawable_get_canvas(self_drawable)));
    
    
    // Skip if its not on screen
    if (pos_x >= canvas_width || (pos_x + width) < 0 || pos_y >= canvas_height || (pos_y + height) < 0)
        return;
    
    
    // Draw child
    gtk_snapshot_save(snap);
    gtk_widget_snapshot_child(GTK_WIDGET(canvas), GTK_WIDGET(self->text_view), snap);
    gtk_snapshot_restore(snap);
}


static void
on_frame_focus_enter(   GnDrawable*     self_drawable,
                        float           x,
                        float           y)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    
    gtk_widget_set_focusable(GTK_WIDGET(self->text_view), TRUE);
    gtk_widget_set_cursor_from_name(GTK_WIDGET(self->text_view), "text");
    gtk_text_view_set_cursor_visible(self->text_view, TRUE);
}


static void
on_frame_focus_exit(    GnDrawable*     self_drawable,
                        float           x,
                        float           y)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    
    gtk_widget_set_focusable(GTK_WIDGET(self->text_view), FALSE);
    if (gtk_widget_is_focus(GTK_WIDGET(self->text_view))) // ..it might crash otherwise
        gtk_text_view_set_cursor_visible(self->text_view, FALSE);
}


static void
on_text_toggle_tag( GnDrawable*     self_drawable,
                    GtkTextTag*     tag,
                    bool            state)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    
    GtkTextBuffer* buf = gtk_text_view_get_buffer(self->text_view);
    
    GtkTextMark* mark_start = gtk_text_buffer_get_mark(buf, "insert");
    GtkTextMark* mark_end = gtk_text_buffer_get_mark(buf, "selection_bound");
    
    GtkTextIter iter_start, iter_end;
    gtk_text_buffer_get_iter_at_mark(buf, &iter_start, mark_start);
    gtk_text_buffer_get_iter_at_mark(buf, &iter_end, mark_end);
    
    if (gtk_text_iter_get_offset(&iter_start) == gtk_text_iter_get_offset(&iter_end))
    {
        while (gtk_text_iter_inside_word(&iter_start))
        {
            if (!gtk_text_iter_backward_char(&iter_start)) break;
        }
        
        while (gtk_text_iter_inside_word(&iter_end))
        {
            if (!gtk_text_iter_forward_char(&iter_end)) break;
        }
        
        if (gtk_text_iter_get_offset(&iter_start) > 0)
            gtk_text_iter_forward_char(&iter_start);
    }
    
    if (state)
        gtk_text_buffer_apply_tag(buf, tag, &iter_start, &iter_end);
    else
        gtk_text_buffer_remove_tag(buf, tag, &iter_start, &iter_end);
}

static void
on_text_bold_toggle(    GnDrawable*     self_drawable,
                        bool            state)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    on_text_toggle_tag(self_drawable, self->tag_bold, state);
}

static void
on_text_italic_toggle(  GnDrawable*     self_drawable,
                        bool            state)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    on_text_toggle_tag(self_drawable, self->tag_italic, state);
}

static void
on_text_underline_toggle(   GnDrawable*     self_drawable,
                            bool            state)
{
    GnDrawableText* self = GN_DRAWABLE_TEXT(self_drawable);
    on_text_toggle_tag(self_drawable, self->tag_underline, state);
}


static void
ctrl_on_motion_enter(   GtkEventControllerMotion*   motion_controller,
                        gdouble                     x,
                        gdouble                     y,
                        gpointer                    user_data)
{
    GnDrawableText* self = (GnDrawableText*)user_data;
    
    if (gn_canvas_get_focused_drawable(gn_drawable_get_canvas(GN_DRAWABLE(self))) == GN_DRAWABLE(self))
        gtk_widget_set_cursor_from_name(GTK_WIDGET(self->text_view), "text");
    else
        gtk_widget_set_cursor(GTK_WIDGET(self->text_view), NULL);
}

static void
ctrl_on_motion_leave(   GtkEventControllerMotion*   motion_controller,
                        gpointer                    user_data)
{
    GnDrawableText* self = (GnDrawableText*)user_data;
    
    gtk_widget_set_cursor(GTK_WIDGET(self->text_view), NULL);
}


static gboolean
frame_tick_callback(    GtkWidget*      txt_widget,
                        GdkFrameClock*  frame_clock,
                        gpointer        user_data)
{
    GnDrawableText* self = (GnDrawableText*)user_data;
    GnDrawable* self_drawable = GN_DRAWABLE(self);
    
    // Get canvas
    GnCanvas* canvas = gn_drawable_get_canvas(self_drawable);
    
    
    // Get position & size
    float pos_x = gn_drawable_get_x(self_drawable);
    float pos_y = gn_drawable_get_y(self_drawable);
    gn_canvas_unit_to_pixel(canvas, &pos_x, &pos_y);
    
    float width = gn_drawable_get_width(self_drawable);
    float height = gn_drawable_get_height(self_drawable);
    
    
    // Update height
    GtkRequisition req;
    gtk_widget_get_preferred_size(GTK_WIDGET(self->text_view), &req, NULL);
    gn_drawable_set_height(self_drawable, req.height);
    
    
    // Get GskTransform
    GskTransform* transform = gsk_transform_new();
    transform = gsk_transform_translate(transform, &GRAPHENE_POINT_INIT(pos_x, pos_y));
    transform = gsk_transform_scale(transform, gn_canvas_get_scale(canvas), gn_canvas_get_scale(canvas));
    
    
    
    // Allocate drawable
    // TODO: only re-allocate if position and/or size has changed?
    gtk_widget_allocate(    GTK_WIDGET(self->text_view),
                            width,
                            height,
                            -1,
                            transform);
    
    gtk_adjustment_set_value(gtk_scrollable_get_vadjustment(GTK_SCROLLABLE(self->text_view)), 0.0);
    
    
    // Update selection bounds
    GtkTextIter start, end;
    gtk_text_buffer_get_selection_bounds(gtk_text_view_get_buffer(self->text_view), &start, &end);
    if (gtk_text_iter_get_line_offset(&start) != self->prev_offset_start || gtk_text_iter_get_line_offset(&end) != self->prev_offset_end)
    {
        self->prev_offset_start = gtk_text_iter_get_line_offset(&start);
        self->prev_offset_end = gtk_text_iter_get_line_offset(&end);
        update_toggle_buttons(self);
    }
    
    return G_SOURCE_CONTINUE;
}



static void
update_toggle_buttons(GnDrawableText* self)
{
    // Get iters
    GtkTextIter start_iter, end_iter;
    gtk_text_buffer_get_selection_bounds(gtk_text_view_get_buffer(self->text_view), &start_iter, &end_iter);
    if (gtk_text_iter_get_offset(&start_iter) != gtk_text_iter_get_offset(&end_iter))
        gtk_text_iter_backward_cursor_position(&end_iter);
    
    // Get iter tag lists
    GSList* start_tag_list = gtk_text_iter_get_tags(&start_iter);
    GSList* end_tag_list = gtk_text_iter_get_tags(&end_iter);
    
    
    // Check bold state
    bool set_bold = FALSE, set_italic = FALSE, set_underline = FALSE;
    for (int i = 0; i < g_slist_length(start_tag_list); i++)
    {
        GtkTextTag* tag = g_slist_nth_data(start_tag_list, i);
        
        
        int weight, style, underline;
        g_object_get(G_OBJECT(tag), "weight", &weight, "style", &style, "underline", &underline, NULL);
        
        if (weight == PANGO_WEIGHT_BOLD || style == PANGO_STYLE_ITALIC || underline == PANGO_UNDERLINE_SINGLE)
        {
            // Check if the end-iter contains the exact same tag
            for (int k = 0; k < g_slist_length(end_tag_list); k++)
            {
                GtkTextTag* ph = g_slist_nth_data(end_tag_list, k);
                
                if (tag == ph)
                {
                    
                    if (weight == PANGO_WEIGHT_BOLD)
                        set_bold = TRUE;
                    else if (style == PANGO_STYLE_ITALIC)
                        set_italic = TRUE;
                    else if (underline == PANGO_UNDERLINE_SINGLE)
                        set_underline = TRUE;
                    
                    break;
                }
            }
        }
    }
    
    
    // Free lists
    g_slist_free(start_tag_list);
    g_slist_free(end_tag_list);
    
    
    // Update button states
    GnGuiController* gui = gn_canvas_get_gui_controller(gn_drawable_get_canvas(GN_DRAWABLE(self)));
    gn_gui_controller_tab_start_set_bold_state(gui, set_bold);
    gn_gui_controller_tab_start_set_italic_state(gui, set_italic);
    gn_gui_controller_tab_start_set_underline_state(gui, set_underline);
}