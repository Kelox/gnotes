#include "common_funcs.h"
#include <math.h>
#include <stdarg.h>


// MACROS //
#define FALSE false
#define TRUE true


// PROTOTYPES //
static float
points_min_max(float* min_x, float* max_x, float* min_y, float* max_y, int point_count, ...);

static float
common_dist_to_line_ext(float   px,
                        float   py,
                        float   line_x1,
                        float   line_y1,
                        float   line_x2,
                        float   line_y2,
                        bool    rounded_end);


// PUBLIC FUNCTIONS //
bool
common_point_inside_rect(   float   point_x,
                            float   point_y,
                            float   rect_x,
                            float   rect_y,
                            float   rect_w,
                            float   rect_h)
{
    return (point_x >= rect_x && point_x < (rect_x + rect_w) && point_y >= rect_y && point_y < (rect_y + rect_h));
}

bool
common_point_inside_triangle(   float   point_x,
                                float   point_y,
                                float   tri_x1,
                                float   tri_y1,
                                float   tri_x2,
                                float   tri_y2,
                                float   tri_x3,
                                float   tri_y3)
{
    return (    common_dist_to_line_ext(point_x, point_y, tri_x1, tri_y1, tri_x2, tri_y2, FALSE) != -1 &&
                common_dist_to_line_ext(point_x, point_y, tri_x1, tri_y1, tri_x3, tri_y3, FALSE) != -1 &&
                common_dist_to_line_ext(point_x, point_y, tri_x3, tri_y3, tri_x2, tri_y2, FALSE) != -1 );
}

bool
common_rect_overlap_rect(   float   r1x,
                            float   r1y,
                            float   r1w,
                            float   r1h,
                            float   r2x,
                            float   r2y,
                            float   r2w,
                            float   r2h)
{
    return (r1x < (r2x + r2w) && (r1x + r1w) > r2x && r1y < (r2y + r2h) && (r1y + r1h) > r2y);
}

float
common_clamp(   float   value,
                float   mini,
                float   maxi)
{
    if (value < mini)
        return mini;
    else if (value > maxi)
        return maxi;
    return value;
}

float
common_dist(    float   x1,
                float   y1,
                float   x2,
                float   y2)
{
    return (float)sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

float
common_dist_to_line(    float   px,
                        float   py,
                        float   line_x1,
                        float   line_y1,
                        float   line_x2,
                        float   line_y2)
{
    return common_dist_to_line_ext(px, py, line_x1, line_y1, line_x2, line_y2, TRUE);
}


void
common_normalize(   float*  x,
                    float*  y)
{
    float len = sqrtf(*x * *x + *y * *y);
    
    if (len != 0)
    {
        *x /= len;
        *y /= len;
    }
}


bool
common_triangle_overlap(    GnVector2   tri1_a,
                            GnVector2   tri1_b,
                            GnVector2   tri1_c,
                            GnVector2   tri2_a,
                            GnVector2   tri2_b,
                            GnVector2   tri2_c)
{
    // Get triangle bounds
    float tri1_minx, tri1_maxx, tri1_miny, tri1_maxy;
    float tri2_minx, tri2_maxx, tri2_miny, tri2_maxy;
    
    points_min_max(&tri1_minx, &tri1_maxx, &tri1_miny, &tri1_maxy, 3, &tri1_a, &tri1_b, &tri1_c);
    points_min_max(&tri2_minx, &tri2_maxx, &tri2_miny, &tri2_maxy, 3, &tri2_a, &tri2_b, &tri2_c);
    
    
    if (tri1_maxx >= tri2_minx && tri1_minx <= tri2_maxx && tri1_maxy >= tri2_miny && tri1_miny <= tri2_maxy)
    {
        GnVector2* triangle[2 * 3];
        triangle[0] = &tri1_a;
        triangle[1] = &tri1_b;
        triangle[2] = &tri1_c;
        triangle[3] = &tri2_a;
        triangle[4] = &tri2_b;
        triangle[5] = &tri2_c;
        
        for (int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                if (common_point_inside_triangle(   triangle[(1 - i) * 3 + k]->x, triangle[(1 - i) * 3 + k]->y,
                                                    triangle[i * 3]->x, triangle[i * 3]->y,
                                                    triangle[i * 3 + 1]->x, triangle[i * 3 + 1]->y,
                                                    triangle[i * 3 + 2]->x, triangle[i * 3 + 2]->y))
                {
                    return TRUE;
                }
            }
        }
    }
    
    return FALSE;
}


bool
common_triangle_overlap_rect(   float   tri_x1,
                                float   tri_y1,
                                float   tri_x2,
                                float   tri_y2,
                                float   tri_x3,
                                float   tri_y3,
                                float   rect_x,
                                float   rect_y,
                                float   rect_width,
                                float   rect_height)
{
    return (common_triangle_overlap(    (GnVector2){ tri_x1, tri_y1 }, (GnVector2){ tri_x2, tri_y2 }, (GnVector2){ tri_x3, tri_y3 },
                                        (GnVector2){ rect_x, rect_y }, (GnVector2){ rect_x + rect_width, rect_y }, (GnVector2){ rect_x, rect_y + rect_height }) || 
            common_triangle_overlap(    (GnVector2){ tri_x1, tri_y1 }, (GnVector2){ tri_x2, tri_y2 }, (GnVector2){ tri_x3, tri_y3 },
                                        (GnVector2){ rect_x + rect_width, rect_y + rect_height }, (GnVector2){ rect_x + rect_width, rect_y }, (GnVector2){ rect_x, rect_y + rect_height }));
}



// PRIVATE FUNCTIONS //
static float
points_min_max(float* min_x, float* max_x, float* min_y, float* max_y, int point_count, ...)
{
    va_list args;
    va_start(args, point_count);
    
    
    *min_x = 99999999;
    *max_x = -99999999;
    *min_y = 99999999;
    *max_y = -99999999;
    
    for (int i = 0; i < point_count; i++)
    {
        GnVector2* point = va_arg(args, GnVector2*);
        
        *min_x = fminf(*min_x, point->x);
        *max_x = fmaxf(*max_x, point->x);
        *min_y = fminf(*min_y, point->y);
        *max_y = fmaxf(*max_y, point->y);
    }
    
    
    va_end(args);
}


static float
common_dist_to_line_ext(float   px,
                        float   py,
                        float   line_x1,
                        float   line_y1,
                        float   line_x2,
                        float   line_y2,
                        bool    rounded_end)
{
    float dx = line_x2 - line_x1;
    float dy = line_y2 - line_y1;
    
    if (dx == 0 && dy == 0)
        goto check_line_point_dist;
    
    
    // Get cut position with normal line
    float a = -dx * (line_x1 - px);
    float b = -dy * (line_y1 - py);
    float c = dx * dx + dy * dy;
    
    float t = (a + b) / c;
    
    float cut_x = line_x1 + t * dx;
    float cut_y = line_y1 + t * dy;
    
    
    // Check if cut is between those points
    float phx1 = fminf(line_x1, line_x2);
    float phx2 = fmaxf(line_x1, line_x2);
    float phy1 = fminf(line_y1, line_y2);
    float phy2 = fmaxf(line_y1, line_y2);
    if (cut_x >= phx1 && cut_x <= phx2 && cut_y >= phy1 && cut_y <= phy2)
        return common_dist(px, py, cut_x, cut_y);
    else
        goto check_line_point_dist;
    
    
    check_line_point_dist:
    if (!rounded_end)
        return -1;
    
    float dist_a = common_dist(px, py, line_x1, line_y1);
    float dist_b = common_dist(px, py, line_x2, line_y2);
    
    return fminf(dist_a, dist_b);
}