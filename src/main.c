#include <adwaita.h>
#include "gn_gui_controller.h"


// PROTOTYPES //
static void
activate(   GtkApplication* app,
            gpointer        user_data);


// VARIABLES //
static GnGuiController* gui_controller;


// FUNCTIONS //
int
main(   int     argc,
        char**  argv)
{
    GtkApplication* app = GTK_APPLICATION(adw_application_new("org.kelox.gnotes", G_APPLICATION_DEFAULT_FLAGS));
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    
    int status = g_application_run(G_APPLICATION(app), argc, argv);
    
    
    g_object_unref(gui_controller);
    g_object_unref(app);
    
    return status;
}


static void
activate(   GtkApplication* app,
            gpointer        user_data)
{
    gui_controller = gn_gui_controller_new(app);
}