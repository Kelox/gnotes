#include "gn_drawable.h"
#include "common_funcs.h"


// PROTOTYPES //
static void
gn_drawable_dispose(GObject* self_object);

static void
gn_drawable_finalize(GObject* self_object);


// EMPTY VIRTUAL METHODS //
static void on_draw(GnDrawable* self) {}
static void on_snapshot(GnDrawable* self, GtkSnapshot* snap) {}

static void on_canvas_append(GnDrawable* self) {}
static void on_canvas_remove(GnDrawable* self) {}

static void on_mouse_enter(GnDrawable* self, float x, float y) {}
static void on_mouse_exit(GnDrawable* self, float x, float y) {}

static void on_mouse_press(GnDrawable* self, int mouse_button, float x, float y) {}
static void on_mouse_release(GnDrawable* self, int mouse_button, float x, float y) {}
static void on_mouse_click(GnDrawable* self, int mouse_button, float x, float y) {}

static void on_mouse_move(GnDrawable* self, float x, float y, float delta_x, float delta_y) {}
static void on_mouse_drag(GnDrawable* self, int mouse_buttons, float x, float y, float delta_x, float delta_y) {}

static void on_frame_move(GnDrawable* self, float old_x, float old_y, float new_x, float new_y) {}
static void on_frame_resize(GnDrawable* self, float old_width, float old_height, float new_width, float new_height) {}

static void on_frame_focus_enter(GnDrawable* self, float x, float y) {}
static void on_frame_focus_exit(GnDrawable* self, float x, float y) {}

static void on_key_press(GnDrawable* self, uint32_t utf8_char) {}
static void on_key_release(GnDrawable* self, uint32_t utf8_char) {}
static void on_key_type(GnDrawable* self, uint32_t utf8_char) {}

static void on_text_bold_toggle(GnDrawable* self, bool state) {}
static void on_text_italic_toggle(GnDrawable* self, bool state) {}
static void on_text_underline_toggle(GnDrawable* self, bool state) {}



// OBJECT CONSTRUCTION //
typedef struct
{
    // Render node
    GskRenderNode* render_node;
    bool use_render_node;
    
    // Allocation
    float x;
    float y;
    float width;
    float height;
    
    // Properties
    bool has_frame; // elements with frames can be dragged around
    bool is_resizable;
    bool is_focusable; // only focused elements can receive input events
    bool always_draw; // if FALSE: check if the drawable-rectangle is visible on screen
    
    // State
    bool was_drawn; // TODO: maybe ignore this and just check whether render_node is NULL or not
    bool has_focus;
    
    // Else
    GnCanvas* canvas;
} GnDrawablePrivate;
G_DEFINE_TYPE_WITH_PRIVATE(GnDrawable, gn_drawable, G_TYPE_OBJECT)

static void
gn_drawable_class_init(GnDrawableClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = gn_drawable_dispose;
    G_OBJECT_CLASS(klass)->finalize = gn_drawable_finalize;
    
    klass->on_draw = &on_draw;
    klass->on_snapshot = &on_snapshot;
    
    klass->on_canvas_append = &on_canvas_append;
    klass->on_canvas_remove = &on_canvas_remove;
    
    klass->on_mouse_enter = &on_mouse_enter;
    klass->on_mouse_exit = &on_mouse_exit;
    
    klass->on_mouse_press = &on_mouse_press;
    klass->on_mouse_release = &on_mouse_release;
    klass->on_mouse_click = &on_mouse_click;
    
    klass->on_mouse_move = &on_mouse_move;
    klass->on_mouse_drag = &on_mouse_drag;
    
    klass->on_frame_move = &on_frame_move;
    klass->on_frame_resize = &on_frame_resize;
    
    klass->on_frame_focus_enter = &on_frame_focus_enter;
    klass->on_frame_focus_exit = &on_frame_focus_exit;
    
    klass->on_key_press = &on_key_press;
    klass->on_key_release = &on_key_release;
    klass->on_key_type = &on_key_type;
    
    klass->on_text_bold_toggle = &on_text_bold_toggle;
    klass->on_text_italic_toggle = &on_text_italic_toggle;
    klass->on_text_underline_toggle = &on_text_underline_toggle;
}

static void
gn_drawable_init(GnDrawable* self)
{
    GnDrawablePrivate* priv = gn_drawable_get_instance_private(self);
    priv->render_node = NULL;
    priv->use_render_node = TRUE;
    
    priv->x = 0;
    priv->y = 0;
    priv->width = 0;
    priv->height = 0;
    
    priv->has_frame = FALSE;
    priv->is_resizable = FALSE;
    priv->is_focusable = FALSE;
    priv->always_draw = FALSE;
    
    priv->was_drawn = FALSE;
    priv->has_focus = FALSE;
    
    priv->canvas = NULL;
}


// OBJECT DECONSTRUCTION //
static void
gn_drawable_dispose(GObject* self_object)
{
    G_OBJECT_CLASS(gn_drawable_parent_class)->dispose(self_object);
}

static void
gn_drawable_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_drawable_parent_class)->finalize(self_object);
}


// METHODS //

void
gn_drawable_draw_frame( GnDrawable*     self,
                        GtkSnapshot*    snap,
                        float           snap_translation_x,
                        float           snap_translation_y,
                        float           snap_scale,
                        float           alpha)
{
    static GdkRGBA shadow_color = (GdkRGBA){ 0, 0, 0, 1 };
    GnDrawablePrivate* priv = gn_drawable_get_instance_private(self);
    
    
    // Translate
    gtk_snapshot_save(snap);
    
    float trans_x = (priv->x - snap_translation_x) * snap_scale;
    float trans_y = (priv->y - snap_translation_y) * snap_scale;
    //gn_canvas_unit_to_pixel(priv->canvas, &trans_x, &trans_y); // ..not using this, as I also want to apply scale outside of the view
    
    gtk_snapshot_translate(snap, &GRAPHENE_POINT_INIT(trans_x, trans_y));
    
    
    // Get cairo
    cairo_t* cairo = gtk_snapshot_append_cairo( snap,
                                                &GRAPHENE_RECT_INIT(
                                                    -GN_DRAWABLE_FRAME_SPACE * 2,
                                                    -GN_DRAWABLE_FRAME_SPACE * 2 - GN_DRAWABLE_FRAME_TITLE_HEIGHT,
                                                    priv->width * snap_scale + GN_DRAWABLE_FRAME_SPACE * 4,
                                                    priv->height * snap_scale + GN_DRAWABLE_FRAME_SPACE * 4 + GN_DRAWABLE_FRAME_TITLE_HEIGHT
                                                ));
    
    // Draw
    cairo_rectangle(    cairo,
                        -GN_DRAWABLE_FRAME_SPACE,
                        -GN_DRAWABLE_FRAME_SPACE,
                        priv->width * snap_scale + GN_DRAWABLE_FRAME_SPACE * 2,
                        priv->height * snap_scale + GN_DRAWABLE_FRAME_SPACE * 2);
    
    cairo_set_source_rgba(cairo, 0.5, 0.5, 0.5, alpha);
    cairo_set_line_width(cairo, 1.5);
    cairo_stroke(cairo);
    
    // ..title
    for (int i = 0; i < 2; i++)
    {
        cairo_rectangle(    cairo,
                            -GN_DRAWABLE_FRAME_SPACE,
                            -GN_DRAWABLE_FRAME_SPACE - GN_DRAWABLE_FRAME_TITLE_HEIGHT,
                            priv->width * snap_scale + GN_DRAWABLE_FRAME_SPACE * 2,
                            GN_DRAWABLE_FRAME_TITLE_HEIGHT);
        
        cairo_set_source_rgba(cairo, 0.5, 0.5, 0.5, alpha * (i == 0 ? 0.3f : 1.0f));
        cairo_set_line_width(cairo, 1.5);
        
        if (i == 0)
            cairo_fill(cairo);
        else
            cairo_stroke(cairo);
    }
    
    
    // Destroy cairo
    cairo_destroy(cairo);
    
    // Restore snap
    gtk_snapshot_restore(snap);
}


bool
gn_drawable_contains_point( GnDrawable* self,
                            float       x,
                            float       y,
                            bool*       is_on_title)
{
    GnDrawablePrivate* priv = gn_drawable_get_instance_private(self);
    
    // Convert pos to pixel
    gn_canvas_unit_to_pixel(priv->canvas, &x, &y);
    
    // Get rect corners
    float topleft_x = priv->x;
    float topleft_y = priv->y;
    gn_canvas_unit_to_pixel(priv->canvas, &topleft_x, &topleft_y);
    topleft_x -= GN_DRAWABLE_FRAME_SPACE;
    topleft_y -= GN_DRAWABLE_FRAME_SPACE + GN_DRAWABLE_FRAME_TITLE_HEIGHT;
    
    float botright_x = priv->x + priv->width;
    float botright_y = priv->y + priv->height;
    gn_canvas_unit_to_pixel(priv->canvas, &botright_x, &botright_y);
    botright_x += GN_DRAWABLE_FRAME_SPACE;
    botright_y += GN_DRAWABLE_FRAME_SPACE;
    
    
    if (is_on_title)
    {
        *is_on_title = common_point_inside_rect(    x,
                                                    y,
                                                    topleft_x,
                                                    topleft_y,
                                                    botright_x - topleft_x,
                                                    GN_DRAWABLE_FRAME_TITLE_HEIGHT);
    }
    
    return common_point_inside_rect(    x,
                                        y,
                                        topleft_x,
                                        topleft_y,
                                        botright_x - topleft_x,
                                        botright_y - topleft_y);
}


bool
gn_drawable_contains_rect(  GnDrawable* self,
                            float       x,
                            float       y,
                            float       width,
                            float       height)
{
    GnDrawablePrivate* priv = gn_drawable_get_instance_private(self);
    
    // Get PASSED rect corners
    float passed_botright_x = x + width;
    float passed_botright_y = y + height;
    gn_canvas_unit_to_pixel(priv->canvas, &passed_botright_x, &passed_botright_y);
    
    gn_canvas_unit_to_pixel(priv->canvas, &x, &y);
    
    
    // Get CANVAS rect corners
    float topleft_x = priv->x;
    float topleft_y = priv->y;
    gn_canvas_unit_to_pixel(priv->canvas, &topleft_x, &topleft_y);
    topleft_x -= GN_DRAWABLE_FRAME_SPACE;
    topleft_y -= GN_DRAWABLE_FRAME_SPACE + GN_DRAWABLE_FRAME_TITLE_HEIGHT;
    
    float botright_x = priv->x + priv->width;
    float botright_y = priv->y + priv->height;
    gn_canvas_unit_to_pixel(priv->canvas, &botright_x, &botright_y);
    botright_x += GN_DRAWABLE_FRAME_SPACE;
    botright_y += GN_DRAWABLE_FRAME_SPACE;
    
    
    return common_rect_overlap_rect(    x,
                                        y,
                                        passed_botright_x - x,
                                        passed_botright_y - y,
                                        topleft_x,
                                        topleft_y,
                                        botright_x - topleft_x,
                                        botright_y - topleft_y);
}



// Get/Set
PRIVATE_GET_SET(GnDrawable, gn_drawable, render_node, GskRenderNode*)
PRIVATE_GET_SET(GnDrawable, gn_drawable, use_render_node, bool)

PRIVATE_GET_SET(GnDrawable, gn_drawable, x, float)
PRIVATE_GET_SET(GnDrawable, gn_drawable, y, float)
PRIVATE_GET_SET(GnDrawable, gn_drawable, width, float)
PRIVATE_GET_SET(GnDrawable, gn_drawable, height, float)

PRIVATE_GET_SET(GnDrawable, gn_drawable, has_frame, bool)
PRIVATE_GET_SET(GnDrawable, gn_drawable, is_resizable, bool)
PRIVATE_GET_SET(GnDrawable, gn_drawable, is_focusable, bool)
PRIVATE_GET_SET(GnDrawable, gn_drawable, always_draw, bool)

PRIVATE_GET_SET(GnDrawable, gn_drawable, was_drawn, bool)
PRIVATE_GET_SET(GnDrawable, gn_drawable, has_focus, bool)

PRIVATE_GET_SET(GnDrawable, gn_drawable, canvas, GnCanvas*)