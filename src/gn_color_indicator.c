#include "gn_color_indicator.h"


// PROTOTYPES //
static void
gn_color_indicator_dispose(GObject* self_object);

static void
gn_color_indicator_finalize(GObject* self_object);

static GtkSizeRequestMode
get_request_mode(GtkWidget* widget);

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline);

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap);


// OBJECT CONSTRUCTION //
struct _GnColorIndicator
{
    GtkWidget parent;
    
    GdkRGBA color;  
};
G_DEFINE_TYPE(GnColorIndicator, gn_color_indicator, GTK_TYPE_WIDGET)

static void
gn_color_indicator_class_init(GnColorIndicatorClass* klass)
{
    G_OBJECT_CLASS(klass)->dispose = &gn_color_indicator_dispose;
    G_OBJECT_CLASS(klass)->finalize = &gn_color_indicator_finalize;
    
    GTK_WIDGET_CLASS(klass)->get_request_mode = &get_request_mode;
    GTK_WIDGET_CLASS(klass)->measure = &measure;
    GTK_WIDGET_CLASS(klass)->snapshot = &snapshot;
}

static void
gn_color_indicator_init(GnColorIndicator* self)
{
    
}


// OBJECT DECONSTRUCTION //
static void
gn_color_indicator_dispose(GObject* self_object)
{
    G_OBJECT_CLASS(gn_color_indicator_parent_class)->dispose(self_object);
}

static void
gn_color_indicator_finalize(GObject* self_object)
{
    G_OBJECT_CLASS(gn_color_indicator_parent_class)->finalize(self_object);
}


// METHODS //
GnColorIndicator*
gn_color_indicator_new( float red,
                        float green,
                        float blue,
                        float alpha)
{
    GnColorIndicator* self = GN_COLOR_INDICATOR(g_object_new(GN_TYPE_COLOR_INDICATOR, NULL));
    
    self->color.red = red;
    self->color.green = green;
    self->color.blue = blue;
    self->color.alpha = alpha;
    
    return self;
}

void
gn_color_indicator_set_color(   GnColorIndicator*   self,
                                float               red,
                                float               green,
                                float               blue,
                                float               alpha)
{
    self->color.red = red;
    self->color.green = green;
    self->color.blue = blue;
    self->color.alpha = alpha;
}

GdkRGBA*
gn_color_indicator_get_color(GnColorIndicator* self)
{
    return &self->color;
}

void
gn_color_indicator_set_button(  GnColorIndicator*   self,
                                GtkToggleButton*    button)
{
    // TODO: attach signal handlers
}


// PRIVATE FUNCTIONS //

static GtkSizeRequestMode
get_request_mode(GtkWidget* widget)
{
    return GTK_SIZE_REQUEST_WIDTH_FOR_HEIGHT;
}

static void
measure(    GtkWidget*      widget,
            GtkOrientation  orientation,
            int             for_size,
            int*            minimum,
            int*            natural,
            int*            minimum_baseline,
            int*            natural_baseline)
{
    if (orientation == GTK_ORIENTATION_HORIZONTAL)
    {
        if (for_size == -1)
        {
            *minimum = 20;
            *natural = 28;
        }
        else
        {
            *natural = for_size + 4;
        }
    }
    else
    {
        if (for_size == -1)
        {
            *minimum = 16;
            *natural = 24;
        }
        else
        {
            *natural = for_size - 4;
        }
    }
}

static void
snapshot(   GtkWidget*      self_widget,
            GtkSnapshot*    snap)
{
    GnColorIndicator* self = GN_COLOR_INDICATOR(self_widget);
    
    // Get size
    float width = gtk_widget_get_allocated_width(self_widget);
    float height = gtk_widget_get_allocated_height(self_widget);
    
    gtk_snapshot_append_color(snap, &self->color, &GRAPHENE_RECT_INIT(0, 0, width, height));
}