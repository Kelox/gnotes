#pragma once

// Lines start off without an GskRenderNode and draw using on_snapshot
// After all points have been collected, gn_drawable_line_end(..) must be called
// Finally, a GskRenderNode will be created & all points will be stored in a "packed" array (instead of a GSList)


// INCLUDES //
#include <adwaita.h>
#include "gn_drawable.h"
#include "common_macros.h"


// TYPE DECLARATION //
#define GN_TYPE_DRAWABLE_LINE gn_drawable_line_get_type()
G_DECLARE_FINAL_TYPE(GnDrawableLine, gn_drawable_line, GN, DRAWABLE_LINE, GnDrawable)


// METHODS //
GnDrawableLine*
gn_drawable_line_new();


// Construction methods

// Adds a new point to the points-list
// Only used during "construction-phase" (e.g. while actively drawing the line)
void
gn_drawable_line_append_point(  GnDrawableLine* self,
                                float           x,
                                float           y);

// Frees the old list & replaces it with the passed one
// Object takes ownership of list
void
gn_drawable_line_set_point_list(    GnDrawableLine* self,
                                    GSList*         list);

// Completes the drawing process & creates a GskRenderNode
// Object takes ownership of list
void
gn_drawable_line_set_point_list_packed( GnDrawableLine* self,
                                        float*          list,
                                        int             list_size);

// Completes the drawing process & creates a GskRenderNode
// The "construction-phase" ends after calling this
void
gn_drawable_line_end(GnDrawableLine* self);

bool
gn_drawable_line_contains_circle(   GnDrawableLine* self,
                                    float           x,
                                    float           y,
                                    float           radius);


// Get/Set

void
gn_drawable_line_set_color( GnDrawableLine* self,
                            float           red,
                            float           green,
                            float           blue,
                            float           alpha);

GdkRGBA*
gn_drawable_line_get_color(GnDrawableLine* self);

PROTOTYPE_GET_SET(GnDrawableLine, gn_drawable_line, line_width, float)