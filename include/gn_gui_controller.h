#pragma once


// INCLUDES //
#include <adwaita.h>


// TYPE DECLARATION //
#define GN_TYPE_GUI_CONTROLLER gn_gui_controller_get_type()
G_DECLARE_FINAL_TYPE(GnGuiController, gn_gui_controller, GN, GUI_CONTROLLER, GObject)


// ENUMS //
enum GnGuiTab
{
    GN_GUI_TAB_START = 1,
    GN_GUI_TAB_INSERT,
    GN_GUI_TAB_DRAW,
    GN_GUI_TAB_VIEW
};

enum GnDrawTool
{
    GN_DRAW_TOOL_PEN = 1,
    GN_DRAW_TOOL_HIGHLIGHTER,
    GN_DRAW_TOOL_SELECTION,
    GN_DRAW_TOOL_ERASER
};


// METHODS //
GnGuiController*
gn_gui_controller_new(GtkApplication* app);

bool
gn_gui_controller_is_tab(   GnGuiController*    self,
                            enum GnGuiTab       id);

enum GnGuiTab
gn_gui_controller_get_tab(GnGuiController* self);

enum GnGuiTab
gn_gui_controller_get_tab_at_page(  GnGuiController*    self,
                                    int                 page_num);

void
gn_gui_controller_set_tab(  GnGuiController*    self,
                            enum GnGuiTab       id);

// Returns a pointer to the currently selected color
GdkRGBA*
gn_gui_controller_get_draw_color(GnGuiController* self);

float
gn_gui_controller_get_draw_width(GnGuiController* self);

enum GnDrawTool
gn_gui_controller_get_draw_tool(GnGuiController* self);



// TAB START PROTOTYPES //

void
gn_gui_controller_tab_start_set_bold_state( GnGuiController*    self,
                                            bool                state);

bool
gn_gui_controller_tab_start_get_bold_state(GnGuiController* self);


void
gn_gui_controller_tab_start_set_italic_state(   GnGuiController*    self,
                                                bool                state);

bool
gn_gui_controller_tab_start_get_italic_state(GnGuiController* self);


void
gn_gui_controller_tab_start_set_underline_state(    GnGuiController*    self,
                                                    bool                state);

bool
gn_gui_controller_tab_start_get_underline_state(GnGuiController* self);