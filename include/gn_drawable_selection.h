#pragma once

// INCLUDES //
#include <adwaita.h>
#include "gn_drawable.h"
#include "common_macros.h"


// TYPE DECLARATION //
#define GN_TYPE_DRAWABLE_SELECTION gn_drawable_selection_get_type()
G_DECLARE_FINAL_TYPE(GnDrawableSelection, gn_drawable_selection, GN, DRAWABLE_SELECTION, GnDrawable)


// METHODS //
GnDrawableSelection*
gn_drawable_selection_new();

void
gn_drawable_selection_add_drawable( GnDrawableSelection*    self,
                                    GnDrawable*             drawable);