#pragma once


// INCLUDES //
#include <adwaita.h>
#include "gn_drawable.h"


// TYPE DECLARATION //
#define GN_TYPE_DRAWABLE_TEXT gn_drawable_text_get_type()
G_DECLARE_FINAL_TYPE(GnDrawableText, gn_drawable_text, GN, DRAWABLE_TEXT, GnDrawable)


// METHODS //
GnDrawableText*
gn_drawable_text_new();

GtkTextView*
gn_drawable_text_get_text_view(GnDrawableText* self);