#pragma once


// INCLUDES //
#include <adwaita.h>
#include "gn_gui_controller.h"


// TYPE DECLARATION //
#define GN_TYPE_CANVAS gn_canvas_get_type()
G_DECLARE_FINAL_TYPE(GnCanvas, gn_canvas, GN, CANVAS, GtkWidget)


// POST-TYPE INCLUDES //
#include "gn_drawable.h"



// METHODS //
GnCanvas*
gn_canvas_new(GnGuiController* gui_controller);

// Appends a new drawable
// Position & size is determined by the drawable itself
void
gn_canvas_append_drawable(  GnCanvas*   self,
                            GnDrawable* drawable);

// Removes a drawable
// Call g_object_unref if you don't need it anymore!!
void
gn_canvas_remove_drawable(  GnCanvas*   self,
                            GnDrawable* drawable);

// You MUST NOT modify this list in any way!
// You do NOT own this list (so don't free it)
// Calling .._append_drawable or .._remove_drawable while working with this list is dangerous
// Only use this to quickly iterate through all drawables
GList*
gn_canvas_get_drawable_list(GnCanvas* self);

// Converts the passed pixel-point (x|y) into scaled units
// (x|y) must be relative to the canvas-widget
void
gn_canvas_pixel_to_unit(    GnCanvas*   self,
                            float*      x,
                            float*      y);

// Converts the passed unit-point (x|y) into its corresponding pixel coordinates
// (x|y) will be relative to the canvas-widget
void
gn_canvas_unit_to_pixel(    GnCanvas*   self,
                            float*      x,
                            float*      y);

GnGuiController*
gn_canvas_get_gui_controller(GnCanvas* self);


// Get/Set
PROTOTYPE_GET_SET(GnCanvas, gn_canvas, translation_x, float)
PROTOTYPE_GET_SET(GnCanvas, gn_canvas, translation_y, float)
PROTOTYPE_GET_SET(GnCanvas, gn_canvas, scale, float)

PROTOTYPE_GETTER(GnCanvas, gn_canvas, hovering_drawable, GnDrawable*)
PROTOTYPE_GETTER(GnCanvas, gn_canvas, focused_drawable, GnDrawable*)