#pragma once


// MACROS //
#define GN_MOUSE_LEFT   0b0001
#define GN_MOUSE_MIDDLE 0b0010
#define GN_MOUSE_RIGHT  0b0100

#define GN_DRAWABLE_FRAME_TITLE_HEIGHT 16
#define GN_DRAWABLE_FRAME_SPACE 4


// INCLUDES //
#include <adwaita.h>
#include "common_macros.h"


// TYPE DECLARATION //
#define GN_TYPE_DRAWABLE gn_drawable_get_type()
G_DECLARE_DERIVABLE_TYPE(GnDrawable, gn_drawable, GN, DRAWABLE, GObject)

struct _GnDrawableClass
{
    GObjectClass parent;
    
    
    void (*on_draw)(GnDrawable* self); // called to (re-)create its GskRenderNode
    void (*on_snapshot)(GnDrawable* self, GtkSnapshot* snap); // only called if use_render_node == FALSE; Snapshot is neither scaled nor translated
    
    void (*on_canvas_append)(GnDrawable* self); // called after the drawable was appended to a canvas
    void (*on_canvas_remove)(GnDrawable* self); // called after the drawable was removed from a canvas
    
    void (*on_mouse_enter)(GnDrawable* self, float x, float y);
    void (*on_mouse_exit)(GnDrawable* self, float x, float y);
    
    void (*on_mouse_press)(GnDrawable* self, int mouse_button, float x, float y);
    void (*on_mouse_release)(GnDrawable* self, int mouse_button, float x, float y);
    void (*on_mouse_click)(GnDrawable* self, int mouse_button, float x, float y);
    
    void (*on_mouse_move)(GnDrawable* self, float x, float y, float delta_x, float delta_y);
    void (*on_mouse_drag)(GnDrawable* self, int mouse_buttons, float x, float y, float delta_x, float delta_y); // multiple mouse buttons may be pressed
    
    void (*on_frame_move)(GnDrawable* self, float old_x, float old_y, float new_x, float new_y);
    void (*on_frame_resize)(GnDrawable* self, float old_width, float old_height, float new_width, float new_height);
    
    void (*on_frame_focus_enter)(GnDrawable* self, float x, float y);
    void (*on_frame_focus_exit)(GnDrawable* self, float x, float y);
    
    void (*on_key_press)(GnDrawable* self, uint32_t utf8_char);
    void (*on_key_release)(GnDrawable* self, uint32_t utf8_char);
    void (*on_key_type)(GnDrawable* self, uint32_t utf8_char);
    
    void (*on_text_bold_toggle)(GnDrawable* self, bool state);
    void (*on_text_italic_toggle)(GnDrawable* self, bool state);
    void (*on_text_underline_toggle)(GnDrawable* self, bool state);
};


// POST-TYPE INCLUDES //
#include "gn_canvas.h"


// METHODS //

// Draws a frame around the drawable
// Snap must neither be scaled nor translated!
void
gn_drawable_draw_frame( GnDrawable*     self,
                        GtkSnapshot*    snap,
                        float           snap_translation_x,
                        float           snap_translation_y,
                        float           snap_scale,
                        float           alpha);


bool
gn_drawable_contains_point( GnDrawable* self,
                            float       x,
                            float       y,
                            bool*       is_on_title);


bool
gn_drawable_contains_rect(  GnDrawable* self,
                            float       x,
                            float       y,
                            float       width,
                            float       height);


// Get/Set
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, render_node, GskRenderNode*)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, use_render_node, bool)

PROTOTYPE_GET_SET(GnDrawable, gn_drawable, x, float)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, y, float)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, width, float)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, height, float)

PROTOTYPE_GET_SET(GnDrawable, gn_drawable, has_frame, bool)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, is_resizable, bool)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, is_focusable, bool)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, always_draw, bool)

PROTOTYPE_GET_SET(GnDrawable, gn_drawable, was_drawn, bool)
PROTOTYPE_GET_SET(GnDrawable, gn_drawable, has_focus, bool)

PROTOTYPE_GET_SET(GnDrawable, gn_drawable, canvas, GnCanvas*)