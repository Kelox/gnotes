#pragma once


// INCLUDES //
#include <adwaita.h>


// TYPE DECLARATION //
#define GN_TYPE_COLOR_INDICATOR gn_color_indicator_get_type()
G_DECLARE_FINAL_TYPE(GnColorIndicator, gn_color_indicator, GN, COLOR_INDICATOR, GtkWidget)


// METHODS //
GnColorIndicator*
gn_color_indicator_new( float red,
                        float green,
                        float blue,
                        float alpha);

void
gn_color_indicator_set_color(   GnColorIndicator*   self,
                                float               red,
                                float               green,
                                float               blue,
                                float               alpha);

GdkRGBA*
gn_color_indicator_get_color(GnColorIndicator* self); 

void
gn_color_indicator_set_button(  GnColorIndicator*   self,
                                GtkToggleButton*    button);