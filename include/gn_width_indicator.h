#pragma once


// INCLUDES //
#include <adwaita.h>
#include "common_macros.h"


// TYPE DECLARATION //
#define GN_TYPE_WIDTH_INDICATOR gn_width_indicator_get_type()
G_DECLARE_FINAL_TYPE(GnWidthIndicator, gn_width_indicator, GN, WIDTH_INDICATOR, GtkWidget)


// METHODS //
GnWidthIndicator*
gn_width_indicator_new( float value,
                        float min_value,
                        float max_value);

float
gn_width_indicator_get_value(GnWidthIndicator* self);

void
gn_width_indicator_set_value(   GnWidthIndicator*   self,
                                float               value);