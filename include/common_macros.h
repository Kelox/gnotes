#ifndef COMMON_MACROS_H
#define COMMON_MACROS_H


// For header files
#define PROTOTYPE_GETTER(ClassName, class_name, member_name, MemberType)    MemberType class_name ## _get_ ## member_name(ClassName* self);

#define PROTOTYPE_SETTER(ClassName, class_name, member_name, MemberType)    void class_name ## _set_ ## member_name(ClassName* self, MemberType member_name);

#define PROTOTYPE_GET_SET(ClassName, class_name, member_name, MemberType)   PROTOTYPE_GETTER(ClassName, class_name, member_name, MemberType) \
                                                                            PROTOTYPE_SETTER(ClassName, class_name, member_name, MemberType)


// For final classes
#define PUBLIC_GETTER(ClassName, class_name, member_name, MemberType)       MemberType class_name ## _get_ ## member_name(ClassName* self) { \
                                                                                return self->member_name; \
                                                                            }

#define PUBLIC_SETTER(ClassName, class_name, member_name, MemberType)       void class_name ## _set_ ## member_name(ClassName* self, MemberType member_name) { \
                                                                                self->member_name = member_name; \
                                                                            }

#define PUBLIC_GET_SET(ClassName, class_name, member_name, MemberType)      PUBLIC_GETTER(ClassName, class_name, member_name, MemberType) \
                                                                            PUBLIC_SETTER(ClassName, class_name, member_name, MemberType)


// For derivable classes
#define PRIVATE_GETTER(ClassName, class_name, member_name, MemberType)      MemberType class_name ## _get_ ## member_name(ClassName* self) { \
                                                                                return ((ClassName ## Private*)gn_drawable_get_instance_private(self))->member_name; \
                                                                            }

#define PRIVATE_SETTER(ClassName, class_name, member_name, MemberType)      void class_name ## _set_ ## member_name(ClassName* self, MemberType member_name) { \
                                                                                ((ClassName ## Private*)gn_drawable_get_instance_private(self))->member_name = member_name; \
                                                                            }

#define PRIVATE_GET_SET(ClassName, class_name, member_name, MemberType)     PRIVATE_GETTER(ClassName, class_name, member_name, MemberType) \
                                                                            PRIVATE_SETTER(ClassName, class_name, member_name, MemberType)


#endif