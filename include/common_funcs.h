#pragma once


// INCLUDES //
#include <stdbool.h>


// STRUCTS //
typedef struct 
{
    float x;
    float y;
} GnVector2;



// PROTOTYPES //
bool
common_point_inside_rect(   float   point_x,
                            float   point_y,
                            float   rect_x,
                            float   rect_y,
                            float   rect_w,
                            float   rect_h);

bool
common_rect_overlap_rect(   float   r1x,
                            float   r1y,
                            float   r1w,
                            float   r1h,
                            float   r2x,
                            float   r2y,
                            float   r2w,
                            float   r2h);

bool
common_point_inside_triangle(   float   point_x,
                                float   point_y,
                                float   tri_x1,
                                float   tri_y1,
                                float   tri_x2,
                                float   tri_y2,
                                float   tri_x3,
                                float   tri_y3);

float
common_clamp(   float   value,
                float   mini,
                float   maxi);

float
common_dist(    float   x1,
                float   y1,
                float   x2,
                float   y2);

float
common_dist_to_line(    float   px,
                        float   py,
                        float   line_x1,
                        float   line_y1,
                        float   line_x2,
                        float   line_y2);

void
common_normalize(   float*  x,
                    float*  y);

bool
common_triangle_overlap(    GnVector2   tri1_a,
                            GnVector2   tri1_b,
                            GnVector2   tri1_c,
                            GnVector2   tri2_a,
                            GnVector2   tri2_b,
                            GnVector2   tri2_c);

bool
common_triangle_overlap_rect(   float   tri_x1,
                                float   tri_y1,
                                float   tri_x2,
                                float   tri_y2,
                                float   tri_x3,
                                float   tri_y3,
                                float   rect_x,
                                float   rect_y,
                                float   rect_width,
                                float   rect_height);